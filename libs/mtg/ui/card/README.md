# [MTG Card](https://mtg.gamepedia.com/Card)

[MTG Card](https://mtg.gamepedia.com/Card) is an [Angular](https://angular.io/) UI library exposing the `CardComponent`.

This component handles displaying of [MTG cards](https://mtg.gamepedia.com/Card) images in given dimensions, with zoom capabilities.

## Usage

To use [MTG Card](https://mtg.gamepedia.com/Card), import the `CardModule` from `@mct/mtg/ui/card` in your requiring module as in the example below:

```typescript
import { CardModule } from '@mct/mtg/ui/card';

@NgModule({
  imports: [CardModule],
})
export class AppModule {}
```
