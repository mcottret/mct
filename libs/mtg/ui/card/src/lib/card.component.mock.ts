import { Component, Input } from '@angular/core';

@Component({
  selector: 'mct-mtg-card',
  template: '',
})
export class CardComponentMock {
  @Input() card;
}
