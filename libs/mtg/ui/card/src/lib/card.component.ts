import {Component, ElementRef, Input, ViewChild} from '@angular/core';

import {Card, CardImageFormat} from '@mct/mtg/util/ts-models';

@Component({
  selector: 'mct-mtg-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() card: Card;

  @ViewChild('imageElement') imageElementRef: ElementRef;

  imageFormat: CardImageFormat = CardImageFormat.Small;
  maxZoomFactor = 2;
  showZoomedImage = false;
  zoomedImageFormat: CardImageFormat = CardImageFormat.Normal;

  zoomedImage: {
    dimensions: {
      width?: number;
      height?: number;
    };

    coordinates: {
      x?: number;
      y?: number;
    };
  } = {
    dimensions: {},
    coordinates: {},
  };

  /**
   * Updates the zoomed image coordinates & dimensions every time it's loaded
   */
  onZoomedImageLoaded() {
    this.updateZoomedImageDimensions();
    this.updateZoomedImageCoordinates();
  }

  /**
   * Toggles display of the zoomed image
   */
  toggleZoomedImageDisplay() {
    this.showZoomedImage = !this.showZoomedImage;
  }

  /**
   * Shows the zoomed image when the image is really hovered
   *
   * Since the `pointerenter` event is also fired on a touch device tap in addition to the `click` event, we need to detect
   *  & exclude 'touch' type events to avoid accidentally setting {@link showZoomedImage} to `true` _before_ toggling it
   *  (because `pointerenter` is fired _before_ `click`), which would cause it to instantly hide
   *
   * See {@link https://stackoverflow.com/questions/39474904/how-to-identify-if-a-mouseover-event-object-came-from-a-touchscreen-touch}
   *
   * @param event
   */
  showZoomedImageOnRealHover(event: PointerEvent) {
    if (event.pointerType !== 'touch') {
      this.showZoomedImage = true;
    }
  }

  /**
   * Retrieves the appropriate image's `cursor` CSS property's value depending on whether its zoomed or not
   */
  getImageCursorCssProperty() {
    return `zoom-${this.showZoomedImage ? 'out' : 'in'}`;
  }

  /**
   * Retrieves the appropriate image's `transform` CSS property's value depending on whether or not the card has a
   *  {@link https://mtg.fandom.com/wiki/Split_card split layout}. Split layout cards are rotated 90° when zoomed for better
   *  readability
   */
  getImageTransformCssProperty() {
    return this.card.isSplit() ? 'rotate(90deg)' : '';
  }

  /**
   * Updates the zoomed images coordinates so that it is centered over the normal image as much as possible while staying
   *  inside the viewport.
   *
   * Since split layout cards are rotated 90° when zoomed, & that the rotation is applied after coordinates calculation,
   *  we have to apply the appropriate offset for split layout cards
   *
   * See {@link getImageTransformCssProperty}
   */
  private updateZoomedImageCoordinates() {
    const imageElement = this.imageElementRef.nativeElement;

    const imageElementCenter = {
      x: imageElement.offsetLeft + imageElement.width / 2,
      y: imageElement.offsetTop + imageElement.height / 2,
    };

    const zoomedImageWidth = this.zoomedImage.dimensions.width;
    const zoomedImageHeight = this.zoomedImage.dimensions.height;

    const documentElement = imageElement.ownerDocument.documentElement;
    let minX = documentElement.scrollLeft;
    let minY = documentElement.scrollTop;

    let maxX = minX + documentElement.clientWidth - zoomedImageWidth;

    let maxY = minY + documentElement.clientHeight - zoomedImageHeight;

    if (this.card.isSplit()) {
      const rotationOffsetX = (zoomedImageHeight - zoomedImageWidth) / 2;
      const rotationOffsetY = (zoomedImageWidth - zoomedImageHeight) / 2;

      minX += rotationOffsetX;
      minY += rotationOffsetY;
      maxX -= rotationOffsetX;
      maxY -= rotationOffsetY;
    }

    this.zoomedImage.coordinates.x = Math.max(
      minX,
      Math.min(imageElementCenter.x - zoomedImageWidth / 2, maxX)
    );

    this.zoomedImage.coordinates.y = Math.max(
      minY,
      Math.min(imageElementCenter.y - zoomedImageHeight / 2, maxY)
    );
  }

  /**
   * Initializes the zoomed image dimensions for the given {@link maxZoomFactor}, or for it to fit the viewport
   */
  private updateZoomedImageDimensions() {
    const imageElement = this.imageElementRef.nativeElement;
    const ratio = imageElement.width / imageElement.height;

    const documentElement = imageElement.ownerDocument.documentElement;
    const maxWidth = this.card.isSplit()
      ? documentElement.clientHeight
      : documentElement.clientWidth;
    const maxHeight = this.card.isSplit()
      ? documentElement.clientWidth
      : documentElement.clientHeight;

    this.zoomedImage.dimensions.width = Math.min(
      imageElement.width * this.maxZoomFactor,
      maxWidth
    );
    this.zoomedImage.dimensions.height = Math.min(
      imageElement.height * this.maxZoomFactor,
      maxHeight
    );

    if (
      this.zoomedImage.dimensions.width / this.zoomedImage.dimensions.height !==
      ratio
    ) {
      if (
        this.zoomedImage.dimensions.width > this.zoomedImage.dimensions.height
      ) {
        this.zoomedImage.dimensions.height =
          this.zoomedImage.dimensions.width / ratio;
      } else {
        this.zoomedImage.dimensions.width =
          this.zoomedImage.dimensions.height * ratio;
      }
    }
  }
}
