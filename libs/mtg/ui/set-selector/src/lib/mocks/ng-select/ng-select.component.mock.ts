import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ng-select', // tslint:disable-line:component-selector
  template: '',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => NgSelectComponentMock),
    },
  ],
})
export class NgSelectComponentMock implements ControlValueAccessor {
  @Input() items;
  @Input() groupBy;
  @Input() loading;
  @Input() multiple;
  @Input() searchFn;
  @Input() clearable;
  @Input() compareWith;
  @Input() hideSelected;
  @Input() closeOnSelect;
  @Input() selectableGroup;
  @Input() maxSelectedItems;
  @Input() selectableGroupAsModel;

  writeValue = jest.fn();
  registerOnChange = jest.fn();
  registerOnTouched = jest.fn();
}
