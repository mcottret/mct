
Since it is using [ng-select](https://www.npmjs.com/package/@ng-select/ng-select), you may need to `@use` one of its themes from your application's SCSS as in the example below (if it does not already):

```scss
@use "@ng-select/ng-select/scss/ant.design.theme.scss";
```

This requires adding `node_modules` to your application `angular.json`'s section `stylePreprocessorOptions` `includePaths` array like in the example below:

```json
{
  "projects": {
    "my-application": {
      "architect": {
        "build": {
          "options": {
            "stylePreprocessorOptions":{
              "includePaths": [
                "libs/mtg/util/styles/src/lib",
                "node_modules",
                ...
              ]
            },
            "extractCss": true,
            ...
          },
          ...
        },
        ...
      },
      ...
    }
  },
  ...
}
```

You may also need to set the `extractCss` option to `true` if it is not already.
