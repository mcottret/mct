import { Component, Input } from '@angular/core';

@Component({
  selector: 'p-checkbox', // tslint:disable-line:component-selector
  template: '',
})
export class PrettyCheckboxComponentMock {
  @Input() value;
  @Input() checked;
}
