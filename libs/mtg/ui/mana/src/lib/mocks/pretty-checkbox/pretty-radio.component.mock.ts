import { Component, Input } from '@angular/core';

@Component({
  selector: 'p-radio', // tslint:disable-line:component-selector
  template: '',
})
export class PrettyRadioComponentMock {
  @Input() value;
  @Input() checked;
}
