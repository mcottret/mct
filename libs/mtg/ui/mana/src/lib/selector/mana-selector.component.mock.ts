import { Component, Input } from '@angular/core';

@Component({
  selector: 'mct-mtg-mana-selector',
  template: '',
})
export class ManaSelectorComponentMock {
  @Input() manaCount;
  @Input() selectedColors;
}
