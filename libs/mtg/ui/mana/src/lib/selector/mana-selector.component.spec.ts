import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ManaSelectorComponent } from './mana-selector.component';
import { PrettyRadioComponentMock } from '../mocks/pretty-checkbox/pretty-radio.component.mock';
import { PrettyToggleComponentMock } from '../mocks/pretty-checkbox/pretty-toggle.component.mock';
import { PrettyCheckboxComponentMock } from '../mocks/pretty-checkbox/pretty-checkbox.component.mock';

describe('ManaSelectorComponent', () => {
  let component: ManaSelectorComponent;
  let fixture: ComponentFixture<ManaSelectorComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule],
        declarations: [
          ManaSelectorComponent,
          PrettyRadioComponentMock,
          PrettyToggleComponentMock,
          PrettyCheckboxComponentMock,
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ManaSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
