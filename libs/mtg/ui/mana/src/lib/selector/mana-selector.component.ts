import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { PrettyCheckBoxChange, PrettyRadioChange } from 'ngx-pretty-checkbox';

import { Colors, ColorSymbol, Mana } from '@mct/mtg/util/ts-models';

@Component({
  selector: 'mct-mtg-mana-selector',
  templateUrl: './mana-selector.component.html',
  styleUrls: ['./mana-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManaSelectorComponent {
  @Input() set manaCount(value) {
    this._manaCount = Math.min(value, this.maximumManaCount);
  }

  @Input() selectedColors: ColorSymbol[] = [];

  @Output() manaChanged: EventEmitter<Mana> = new EventEmitter<Mana>();

  colors = Colors;
  _manaCount = 0;
  maximumManaCount = 7;

  /**
   * @param manaCount
   */
  getManaCountTitle(manaCount: number) {
    return `${manaCount === this.maximumManaCount ? 'Infinite ' : ''}CMC${
      manaCount < this.maximumManaCount ? ` <= ${manaCount}` : ''
    }`;
  }

  /**
   * Retrieves an array of selectable mana count values (from 1 to [this.maximumManaCount]{@link ManaSelectorComponent#maximumManaCount})
   *
   * @returns {number[]}
   */
  getManaCounts(): number[] {
    const manaCounts = [];

    for (let i = 1; i <= this.maximumManaCount; ++i) {
      manaCounts.push(i);
    }

    return manaCounts;
  }

  /**
   * Updates the current {@link manaCount mana count} & emits a new {@link Mana} whenever the selected {@link manaCount mana count} changes
   *
   * @param $event
   */
  onManaCountChanged($event: PrettyRadioChange): void {
    this._manaCount = parseInt($event.value, 10);
    this.emitManaChanged();
  }

  /**
   * Updates the currently {@link selectedColors selected colors} & emits a new {@link Mana} whenever the {@link selectedColors selected colors} change
   *
   * @param $event
   */
  onColorsChanged($event: PrettyCheckBoxChange): void {
    if ($event.checked) {
      this.selectedColors.push($event.value as ColorSymbol);
    } else {
      this.selectedColors.splice(
        this.selectedColors.indexOf($event.value as ColorSymbol),
        1
      );
    }

    this.emitManaChanged();
  }

  /**
   * Computes & emits the current {@link Mana}
   */
  emitManaChanged(): void {
    this.manaChanged.emit(this.getMana());
  }

  /**
   * Retrieves the {@link Mana} corresponding to currently selected {@link _manaCount} & {@link selectedColors}
   */
  getMana(): Mana {
    const mana = new Mana();
    mana.colors = this.selectedColors;

    /**
     * If the [maximum amount of mana]{@link maximumManaCount} is selected, it counts as an infinite amount of mana
     *  (which corresponds to leaving the [Mana's cmc]{@link Mana#cmc} unset)
     */
    if (this._manaCount !== this.maximumManaCount) {
      mana.cmc = this._manaCount;
    }

    /**
     * If all the colors are selected, leave the [mana.waysToFulfill]{@link Mana#waysToFulfill} unset, which indicates that
     *  it can fulfill anything
     */
    if (this._manaCount && mana.colors.length && mana.colors.length < 5) {
      const coloredManaSymbols: ColorSymbol[][] = [];

      for (let i = 0; i < this._manaCount; ++i) {
        coloredManaSymbols.push(mana.colors);
      }

      mana.setWaysToFulfill(coloredManaSymbols);
    }

    return mana;
  }

  /**
   * Returns the title attribute for the Mana Count {@param i}, or 'Any CMC' if it is the last one (infinite)
   *
   * @param i
   * @param last
   */
  getManaCountTitle(i: number, last: boolean) {
    return `${last ? 'Any ' : ''}CMC${last ? '' : ` <= ${i}`}`;
  }
}
