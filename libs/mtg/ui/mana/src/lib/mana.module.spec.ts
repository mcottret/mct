import { TestBed, waitForAsync } from '@angular/core/testing';

import { ManaModule } from './mana.module';

describe('ManaModule', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [ManaModule],
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(ManaModule).toBeDefined();
  });
});
