import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';

import { ManaSelectorComponent } from './selector/mana-selector.component';

@NgModule({
  imports: [CommonModule, FormsModule, NgxPrettyCheckboxModule],
  exports: [ManaSelectorComponent],
  declarations: [ManaSelectorComponent],
})
export class ManaModule {}
