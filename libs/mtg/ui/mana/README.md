# [MTG Mana](https://mtg.gamepedia.com/Mana)

[MTG Mana](https://mtg.gamepedia.com/Mana) is an [Angular](https://angular.io/) UI library exposing the `ManaSelectorComponent`.

This component displays a list of [colors](https://mtg.gamepedia.com/Color), each with an associated [number input](https://www.w3schools.com/tags/att_input_type_number.asp) filled by the user. Everytime a value changes, it emits a [Mana](../libs/mtg/util/ts-models/src/lib/mana/mana.class.ts) corresponding to the selected values.

## Dependencies

_Styles:_

- `node_modules/pretty-checkbox/dist/pretty-checkbox.css`
- [MTG Styles](/libs/mtg/util/styles/README.md)

_Assets:_

- [MTG Assets](/libs/mtg/util/assets/README.md)

## Usage

To use [MTG Mana](https://mtg.gamepedia.com/Mana), import the `ManaModule` from `@mct/mtg/ui/mana` in your requiring module as in the example below:

```typescript
import { ManaModule } from '@mct/mtg/ui/mana';

@NgModule({
  imports: [ManaModule],
})
export class AppModule {}
```

Since it is using [NgxPrettyCheckbox](https://www.npmjs.com/package/ngx-pretty-checkbox) & the [MTG Assets](/libs/mtg/util/assets/README.md), you may need to add their styles & assets to your application `angular.json`'s section appropriate arrays as in the example below (if it does not already):

```json
{
  "projects": {
    "my-application": {
      "architect": {
        "build": {
          "options": {
            "styles": [
              "node_modules/pretty-checkbox/dist/pretty-checkbox.css",
              ...
            ],
            "assets": [
              {
                "input": "libs/mtg/util/assets/src/assets",
                "glob": "**/*",
                "output": "assets"
              },
              ...
            ],
            ...
          },
          ...
        },
        ...
      },
      ...
    }
  },
  ...
}
```
