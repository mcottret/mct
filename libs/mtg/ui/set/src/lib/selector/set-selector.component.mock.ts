import { Component, Input } from '@angular/core';

@Component({
  selector: 'mct-mtg-set-selector',
  template: '',
})
export class SetSelectorComponentMock {
  @Input() sets;
  @Input() loading;
  @Input() selected;
  @Input() maximumSelected;
}
