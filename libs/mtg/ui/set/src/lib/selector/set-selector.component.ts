import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { isEqualWith } from 'lodash-es';

import { Set } from '@mct/mtg/util/ts-models';

@Component({
  selector: 'mct-mtg-set-selector',
  templateUrl: './set-selector.component.html',
  styleUrls: ['./set-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SetSelectorComponent {
  @Input() sets: Set[] = [];
  @Input() loading = false;
  @Input() selected: Set[] = [];
  @Input() maximumSelected = 10;

  @Output() setsChanged: EventEmitter<Set[]> = new EventEmitter<Set[]>();

  /**
   * Returns a boolean indicating whether or not the two given sets are equal. {@link https://lodash.com/docs/4.17.15#isEqualWith Lodash's `isEqualWith`}
   *  is used with {@link Set#isEqualWithCustomizer Set.isEqualWithCustomizer} because {@param A}, {@param B}, or both could
   *  be `null`, or pointing to two different references while having the same value, & that these values should still
   *  be considered equal.
   *
   * @param A
   * @param B
   */
  compareSets(A: Set, B: Set) {
    return isEqualWith(A, B, Set.isEqualWithCustomizer);
  }

  /**
   * {@link https://www.npmjs.com/package/@ng-select/ng-select ng-select}'s `searchFn` implementation, returns `true` if
   *  the given set matches the given search term
   *
   * @param searchTerm
   * @param set
   */
  searchFn(searchTerm: string, set: Set) {
    return set.matchesSearchTerm(searchTerm);
  }

  emitSetsChanged() {
    this.setsChanged.emit(this.selected);
  }
}
