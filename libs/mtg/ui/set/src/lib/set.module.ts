import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgSelectModule } from '@ng-select/ng-select';

import { SetSelectorComponent } from './selector/set-selector.component';

@NgModule({
  imports: [CommonModule, FormsModule, NgSelectModule],
  exports: [SetSelectorComponent],
  declarations: [SetSelectorComponent],
})
export class SetModule {}
