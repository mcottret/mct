# [MTG Set](https://mtg.gamepedia.com/Set)

[MTG Set](https://mtg.gamepedia.com/Set) is an [Angular](https://angular.io/) UI library exposing the `SetSelectorComponent`.

## Dependencies

_Styles:_

- `node_modules/ng-select/ng-select/scss/*.theme.scss`
- [Shared Styles](/libs/shared/util/styles/README.md)

## Usage

To use [MTG Set](https://mtg.gamepedia.com/Set), import the `SetModule` from `@mct/mtg/ui/set` in your requiring module as in the example below:

```typescript
import { SetModule } from '@mct/mtg/ui/set';

@NgModule({
  imports: [SetModule],
})
export class AppModule {}
```
