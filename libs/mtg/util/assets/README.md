# MTG Assets

MTG Assets is an utility library exposing MTG assets files & utility functions.

## Usage

To use MTG Assets, add its assets to your application `angular.json`'s section `assets` array like in the example below:

```json
{
  "projects": {
    "my-application": {
      "architect": {
        "build": {
          "options": {
            "assets": [
              {
                "input": "libs/mtg/util/assets/src/assets",
                "glob": "**/*",
                "output": "assets"
              },
              ...
            ],
            ...
          },
          ...
        },
        ...
      },
      ...
    }
  },
  ...
}
```
