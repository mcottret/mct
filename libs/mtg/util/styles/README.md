# MTG Styles

MTG Styles is an utility library exposing MTG-related mixins, variables, components, layouts & themes stylesheets.

## Dependencies

_Styles:_

- `node_modules/mana-font/sass/mana.scss`
- `node_modules/keyrune/sass/keyrune.scss`
- [Shared Styles](/libs/shared/util/styles/README.md)

_Assets:_

- [MTG Assets](/libs/mtg/util/assets/README.md)

## Usage

To use MTG Styles, add its include path to your application `angular.json`'s section `stylePreprocessorOptions` `includePaths` array.

Since it is itself `@use`-ing [Keyrune](https://keyrune.andrewgioia.com/)'s & the [Mana font](https://github.com/andrewgioia/Mana) [SASS stylesheets](/libs/mtg/util/styles/src/lib/vendors) with `node_modules`-relative paths, it is also required to add `node_modules` as in the example below.

```json
{
  "projects": {
    "my-application": {
      "architect": {
        "build": {
          "options": {
            "stylePreprocessorOptions":{
              "includePaths": [
                "libs/mtg/util/styles/src/lib",
                "node_modules",
                ...
              ]
            },
            "extractCss": true,
            ...
          },
          ...
        },
        ...
      },
      ...
    }
  },
  ...
}
```

You may also need to set the `extractCss` option to `true` if it is not already.

Then, import its `mtg-main.scss` file from your application's `styles.scss` file like in the example below:

```scss
@use "mtg-main.scss";
// ...
```
