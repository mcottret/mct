import { SetType } from './set-type.enum';

export interface SetInterface {
  name: string;
  code: string;

  type: SetType;

  cardCount?: number;
  searchUri?: URL;
  releaseDate?: Date;
  draftBoosterSearchUri?: URL;
}
