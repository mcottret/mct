import { SetCode } from './set-code.enum';
import { SetType } from './set-type.enum';
import { SetInterface } from './set.interface';

export class Set {
  /**
   * The name of the set
   */
  name: string;

  /**
   * The code name of the set
   */
  code: SetCode;

  /**
   * Type of set
   */
  type: SetType;

  /**
   * The {@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time UTC} date at which the set was released. For promo sets, the {@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time UTC} date at which the first card was released
   */
  releaseDate: Date;

  /**
   * The name of this set's {@link https://mtg.fandom.com/wiki/Block block}
   */
  block?: string;

  /**
   * The code of this set's {@link https://mtg.fandom.com/wiki/Block block}
   */
  blockCode?: string;

  /**
   * The number of cards in this set
   */
  cardCount?: number;

  /**
   * An API URI that can be requested to begin paginating over the cards in this set
   */
  searchUri?: URL;

  /**
   * An API URI that can be requested to begin paginating over the cards found in draft boosters for this set
   */
  draftBoosterSearchUri?: URL;

  /**
   * {@link https://lodash.com/docs/4.17.15#isEqualWith Lodash's `isEqualWith`} customizer function for sets comparison,
   *  if both A & B are Set objects, returns a boolean indicating whether or not they are equal, returns `undefined` otherwise
   *
   * @param A
   * @param B
   */
  static isEqualWithCustomizer(A: Set, B: Set) {
    if (A && B) {
      return A.code === B.code;
    }
  }

  /**
   * @param {SetInterface} set
   */
  constructor(set?: SetInterface) {
    if (set) {
      this.parse(set);
    }
  }

  /**
   * Whether or not the set is officially draftable
   *
   * @returns {boolean}
   */
  isDraftable() {
    return [
      SetType.Core,
      SetType.Expansion,
      SetType.Masters,
      SetType.DraftInnovation,
    ].includes(this.type);
  }

  /**
   * Returns the timestamp at which the set was released
   *
   * @returns {number}
   */
  getReleaseTimestamp() {
    return this.releaseDate.getTime();
  }

  /**
   * Whether or not the set has been released as of today
   *
   * @returns {boolean}
   */
  isReleased() {
    return this.wasReleasedBefore(Date.now());
  }

  /**
   * Whether or not the set was released before the given timestamp
   *
   * @param {number} timestamp
   * @returns {boolean}
   */
  wasReleasedBefore(timestamp: number) {
    return this.getReleaseTimestamp() < timestamp;
  }

  /**
   * Whether or not the set was released after the given timestamp
   *
   * @param {number} timestamp
   * @returns {boolean}
   */
  wasReleasedAfter(timestamp: number) {
    return this.getReleaseTimestamp() > timestamp;
  }

  /**
   * Basic search term matching, returns `true` if this set's name, code, block or block code matches the given
   *  search term, in a case-insensitive manner
   *
   * @param term
   */
  matchesSearchTerm(term: string) {
    term = term.toLowerCase();

    return (
      this.name.toLowerCase().includes(term) ||
      this.code.toLowerCase().includes(term) ||
      this.block?.toLowerCase().includes(term) ||
      this.blockCode?.toLowerCase().includes(term)
    );
  }

  /**
   * Parses a given SetInterface into this instance
   *
   * @param {SetInterface} set
   */
  private parse(set: SetInterface) {
    for (const key in set) {
      if (set.hasOwnProperty(key)) {
        this[key] = set[key];
      }
    }
  }
}
