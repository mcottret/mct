export enum SetCode {
  Strixhaven = 'stx',
  StrixhavenMysticalArchive = 'sta',
}
