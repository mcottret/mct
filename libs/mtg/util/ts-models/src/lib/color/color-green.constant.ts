import { Color } from './color.interface';
import { ColorSymbol } from './color-symbol.enum';

export const GreenColor: Color = {
  name: 'Green',
  symbol: ColorSymbol.Green,
};
