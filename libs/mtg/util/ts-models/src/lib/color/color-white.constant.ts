import { Color } from './color.interface';
import { ColorSymbol } from './color-symbol.enum';

export const WhiteColor: Color = {
  name: 'White',
  symbol: ColorSymbol.White,
};
