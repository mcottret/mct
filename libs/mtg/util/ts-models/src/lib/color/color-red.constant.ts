import { Color } from './color.interface';
import { ColorSymbol } from './color-symbol.enum';

export const RedColor: Color = {
  name: 'Red',
  symbol: ColorSymbol.Red,
};
