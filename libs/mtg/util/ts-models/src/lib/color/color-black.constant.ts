import { Color } from './color.interface';
import { ColorSymbol } from './color-symbol.enum';

export const BlackColor: Color = {
  name: 'Black',
  symbol: ColorSymbol.Black,
};
