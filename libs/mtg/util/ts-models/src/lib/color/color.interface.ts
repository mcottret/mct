import { ColorSymbol } from './color-symbol.enum';

export interface Color {
  name: string;
  symbol: ColorSymbol;
}
