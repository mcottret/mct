export enum ColorSymbol {
  Red = 'R',
  Blue = 'U',
  Black = 'B',
  Green = 'G',
  White = 'W',
}
