import { Color } from './color.interface';
import { RedColor } from './color-red.constant';
import { BlueColor } from './color-blue.constant';
import { BlackColor } from './color-black.constant';
import { GreenColor } from './color-green.constant';
import { WhiteColor } from './color-white.constant';

export const Colors: Color[] = [
  RedColor,
  BlueColor,
  BlackColor,
  GreenColor,
  WhiteColor,
];
