import { Color } from './color.interface';
import { ColorSymbol } from './color-symbol.enum';

export const BlueColor: Color = {
  name: 'Blue',
  symbol: ColorSymbol.Blue,
};
