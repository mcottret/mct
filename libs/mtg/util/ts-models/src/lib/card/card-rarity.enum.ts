export enum CardRarity {
  Rare = 'RARE',
  Mythic = 'MYTHIC',
  Common = 'COMMON',
  Special = 'SPECIAL',
  Uncommon = 'UNCOMMON',
  BasicLand = 'BASIC_LAND',
  MythicRare = 'MYTHIC_RARE',
}
