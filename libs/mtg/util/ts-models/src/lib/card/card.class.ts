import { $enum } from 'ts-enum-util';

import { Mana } from '../mana/mana.class';
import { SetCode } from '../set/set-code.enum';
import { CardType } from './card-type.enum';
import { CardLayout } from './card-layout.enum';
import { CardRarity } from './card-rarity.enum';
import { ColorSymbol } from '../color/color-symbol.enum';
import { CardSupertype } from './card-supertype.enum';
import { CardInterface } from './card.interface';
import { CardImageFormat } from './card-image-format.enum';
import { CardKeywordAbility } from './keyword-ability/card-keyword-ability.class';
import { CardKeywordAbilityEnum } from './keyword-ability/card-keyword-ability.enum';

export class Card {
  /**
   * The [card name]{@link https://mtg.gamepedia.com/Name}. For [split]{@link https://mtg.gamepedia.com/Split_card}, [double-faced]{@link https://mtg.gamepedia.com/Double-faced_card} and [flip]{@link https://mtg.gamepedia.com/Flip_card} cards, just the name of one side of the card.
   *     Basically each 'sub-card' has its own record
   */
  name: string;

  /**
   * The code of the [set]{@link https://mtg.fandom.com/wiki/Set} this card belongs to
   */
  setCode: SetCode;

  /**
   * The card's [type line]{@link https://mtg.gamepedia.com/Type_line}. This is the type you would see on the card if printed today.
   *
   * Note: The dash is a UTF8 'long dash' as per the MTG rules
   */
  typeLine: string;

  /**
   * The [subtypes]{@link https://mtg.gamepedia.com/Subtype} of the card. These appear to the right of the dash in a card type. Usually each word is
   *     its own subtype.
   */
  subtypes: string[];

  /**
   * The [types]{@link https://mtg.gamepedia.com/Card_type} of the card. These appear to the left of the dash in a card's [type line]{@link https://mtg.gamepedia.com/Type_line}
   */
  types: CardType[];

  /**
   * The rarity of the card
   */
  rarity: CardRarity;

  /**
   * The card layout
   */
  layout: CardLayout;

  /**
   * This card's [mana cost]{@link https://mtg.gamepedia.com/Mana_cost}
   */
  manaCost: Mana;

  /**
   * The [supertypes]{@link https://mtg.gamepedia.com/Supertype} of the card. These appear to the far left of the card type
   */
  supertypes: CardSupertype[];

  /**
   * The card's [color identity]{@link https://mtg.gamepedia.com/Color_identity}, by color code. `["Red", "Blue"]` becomes `["R", "U"]`. A card’s [color identity]{@link https://mtg.gamepedia.com/Color_identity}
   *     includes colors from the card’s rules text
   */
  colorIdentity: ColorSymbol[];

  /**
   * The [keyword abilities]{@link https://mtg.gamepedia.com/Keyword_ability} of the card
   *
   * See:
   *  - {@link CardKeywordAbility}
   *  - {@link https://mtg.gamepedia.com/Keyword_ability}
   */
  keywordAbilities: CardKeywordAbility[] = [];

  /**
   * The [power]{@link https://mtg.gamepedia.com/Power/Toughness} of the card. This is only present for [creatures]{@link https://mtg.gamepedia.com/Creature}. This is a string, not an number, because
   *     some cards have powers like "1+*"
   */
  power?: string;

  /**
   * The [loyalty]{@link https://mtg.gamepedia.com/Loyalty} of the card. This is only present for [planeswalkers]{@link https://magic.wizards.com/fr/game-info/story/planeswalkers/how-planeswalker-cards-work}
   */
  loyalty?: string;

  /**
   * The [toughness]{@link https://mtg.gamepedia.com/Power/Toughness} of the card. This is only present for [creatures]{@link https://mtg.gamepedia.com/Creature}. This is a string, not an number,
   *     because some cards have toughness like: "1+*"
   */
  toughness?: string;

  /**
   * The [oracle]{@link https://mtg.gamepedia.com/Oracle} text of the card. May contain [mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} and other symbols
   */
  oracleText?: string;

  /**
   * Maximum [hand size modifier]{@link https://mtg.gamepedia.com/Hand_and_life_modifier}. Only exists for [Vanguard]{@link https://magic.wizards.com/fr/vanguard} cards
   */
  handModifier?: string;

  /**
   * Starting [life total modifier]{@link https://mtg.gamepedia.com/Hand_and_life_modifier}. Only exists for [Vanguard]{@link https://magic.wizards.com/fr/vanguard} cards
   */
  lifeModifier?: string;

  /**
   * The card's faces, which could be shown divided on the front of the card as in {@link https://mtg.gamepedia.com/Split_card split} and {@link https://mtg.gamepedia.com/Flip_card flip} cards, or on each side as in {@link https://mtg.gamepedia.com/Double-faced_card} double-faced cards}.
   */
  faces?: Card[];

  /**
   * The card [colors]{@link https://mtg.gamepedia.com/Color}. Usually this is derived from the [casting cost]{@link https://mtg.gamepedia.com/Mana_cost}, but some cards are special (like the
   *     back of dual sided cards and [Ghostfire]{@link https://gatherer.wizards.com/pages/card/Details.aspx?multiverseid=136044})
   */
  colors?: ColorSymbol[];

  /**
   * The [UTC]{@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time} date at which this card was released. This is only set for promo cards. The date may not be
   *     accurate to an exact day and month, thus only a partial date may be set. Some promo cards do not have a known
   *     release date
   */
  releaseDate?: Date;

  /**
   * An object listing available imagery for this card in [Scryfall Card Imagery format]{@link https://scryfall.com/docs/api/images}
   *
   * See {@link https://scryfall.com/docs/api/images}
   */
  imagesUris?: {
    [format in CardImageFormat]: URL;
  };

  /**
   * @param {CardInterface} card
   */
  constructor(card?: CardInterface) {
    if (card) {
      this.parse(card);
    }
  }

  /**
   * Returns a boolean indicating whether or not this card can be played for the given {@link Mana}
   *
   * @param mana
   * @param keywordAbilities
   *
   * @returns {boolean}
   */
  isPlayable(
    mana: Mana,
    keywordAbilities: CardKeywordAbilityEnum[] = []
  ): boolean {
    let isPlayable = false;

    if (this.faces) {
      isPlayable = this.faces.some((cardFace) =>
        cardFace.isPlayable(mana, keywordAbilities)
      );
    } else {
      let i = 0;

      while (!isPlayable && i < keywordAbilities.length) {
        isPlayable = this.isPlayableWithKeywordAbility(
          keywordAbilities[i],
          mana
        );
        ++i;
      }

      isPlayable = isPlayable || this.manaCost.canBeFulfilledWith(mana);
    }

    return isPlayable;
  }

  /**
   * Returns a boolean indicating whether or not this card can be played with the given [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability} for the given {@link Mana}
   *
   * @param mana
   * @param {CardKeywordAbilityEnum} keyword
   *
   * @returns {boolean}
   */
  isPlayableWithKeywordAbility(keyword: CardKeywordAbilityEnum, mana: Mana) {
    const keywordAbility = this.getKeywordAbility(keyword);

    return (
      keywordAbility &&
      keywordAbility.manaCost &&
      keywordAbility.manaCost.canBeFulfilledWith(mana)
    );
  }

  /**
   * Returns a boolean indicating whether or not this card can be played at "[Instant speed]{@link https://mtg.gamepedia.com/Instant#Instant_speed}"
   *
   * See:
   *  - {@link https://mtg.gamepedia.com/Cycling}
   *
   * @returns {boolean}
   */
  hasInstantSpeed(): boolean {
    return this.isInstant() || this.hasFlash() || this.hasCycling();
  }

  /**
   * Returns a boolean indicating whether or not this card is of the "{@link https://mtg.gamepedia.com/Instant Instant}" type
   *
   * See [CardType.Instant]{@link CardType#Instant}
   *
   * @returns {boolean}
   */
  isInstant(): boolean {
    return this.types.includes(CardType.Instant);
  }

  /**
   * Whether or not this card has the [CardKeywordAbilityEnum.Flash]{@link CardKeywordAbilityEnum#Flash} [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability}
   *
   * @returns {boolean}
   */
  hasFlash(): boolean {
    return (
      this.hasKeywordAbility(CardKeywordAbilityEnum.Flash) ||
      (this.oracleText &&
        this.oracleText.includes(
          'This spell has flash as long as you control a permanent with flash.'
        ))
    );
  }

  /**
   * Whether or not this card has the [CardKeywordAbilityEnum.Cycling]{@link CardKeywordAbilityEnum#Cycling} [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability}. If the `withEffect` parameter is
   *  set to `true`, only returns `true` if the card also triggers an effect when cycled
   *
   * @param {boolean} withEffect
   *
   * @returns {boolean}
   */
  hasCycling(withEffect: boolean = false): boolean {
    let hasCycling = this.hasKeywordAbility(CardKeywordAbilityEnum.Cycling);

    if (withEffect) {
      hasCycling =
        hasCycling && this.oracleText.includes(`When you cycle ${this.name}`);
    }

    return hasCycling;
  }

  /**
   * Returns a boolean indicating whether or not this card has the [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability} with the given keyword
   *
   * @param {CardKeywordAbilityEnum} keyword
   *
   * @returns {boolean}
   */
  hasKeywordAbility(keyword: CardKeywordAbilityEnum): boolean {
    return !!this.getKeywordAbility(keyword);
  }

  /**
   * Retrieves the given [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability} of this card if it has it, undefined otherwise
   *
   * @param {CardKeywordAbilityEnum} keyword
   *
   * @returns {CardKeywordAbility}
   */
  getKeywordAbility(keyword: CardKeywordAbilityEnum): CardKeywordAbility {
    return this.keywordAbilities.find(
      (keywordAbility) => keywordAbility.keyword === keyword
    );
  }

  /**
   * Retrieves this card's front face image URI in the given format
   *
   * @param format
   *
   * @returns {string}
   */
  getFrontFaceImageUri(format: CardImageFormat): string {
    const uri = this.isDoubleFaced()
      ? this.faces[0].imagesUris[format]
      : this.imagesUris[format];

    return uri.href;
  }

  /**
   * Returns a boolean indicating whether or not this card is [double-faced]{@link https://mtg.gamepedia.com/Double-faced_card}
   */
  isDoubleFaced() {
    return this.layout === CardLayout.DoubleFaced;
  }

  /**
   * Returns a boolean indicating whether or not this card is [split]{@link https://mtg.fandom.com/wiki/Split_card}
   */
  isSplit() {
    return this.layout === CardLayout.Split;
  }

  /**
   * Parses a given {@link CardInterface} into this instance
   *
   * @param {CardInterface} card
   */
  private parse(card: CardInterface) {
    for (const key in card) {
      if (card.hasOwnProperty(key)) {
        this[key] = card[key];
      }
    }

    this.parseOracleText(card.oracleText);
  }

  /**
   * Parses this card's [keyword abilities](https://mtg.gamepedia.com/Keyword_ability) from its [Oracle](https://mtg.gamepedia.com/Oracle) text
   *
   * @param oracleText
   */
  private parseOracleText(oracleText: string) {
    if (oracleText) {
      const lines = oracleText.split('\n');

      lines.forEach((line) => {
        const parts = line.split(' ', 2) as [
          CardKeywordAbilityEnum,
          string?,
          string?
        ];

        if ($enum(CardKeywordAbilityEnum).isValue(parts[0])) {
          this.keywordAbilities.push(new CardKeywordAbility(parts));
        }
      });
    }
  }
}
