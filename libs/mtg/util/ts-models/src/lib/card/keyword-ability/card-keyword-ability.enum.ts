export enum CardKeywordAbilityEnum {
  Flash = 'Flash',
  Mutate = 'Mutate',
  Cycling = 'Cycling',
  Foretell = 'Foretell',
}
