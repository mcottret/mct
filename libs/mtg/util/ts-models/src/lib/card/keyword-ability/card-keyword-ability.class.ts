import { Mana } from '../../mana/mana.class';
import { CardKeywordAbilityEnum } from './card-keyword-ability.enum';

export class CardKeywordAbility {
  /**
   * The [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability}'s keyword
   */
  keyword: CardKeywordAbilityEnum;

  /**
   * The keyword ability's [mana cost]{@link https://mtg.gamepedia.com/Mana_cost}, if it has one
   */
  manaCost?: Mana;

  /**
   * The [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability}'s [reminder text]{@link https://mtg.gamepedia.com/Reminder_text}, if it has one
   */
  reminderText?: string;

  /**
   * @param {[CardKeywordAbility , string , string]} parts
   */
  constructor(parts?: [CardKeywordAbilityEnum, string?, string?]) {
    if (parts) {
      this.parse(parts);
    }
  }

  /**
   * Parses given parts array into this instance, parts array results from splitting a card's [keyword ability]{@link https://mtg.gamepedia.com/Keyword_ability} line on the
   *  space character, which can be either 3 parts containing a keyword, [mana cost]{@link https://mtg.gamepedia.com/Mana_cost} string & reminder text, 2 parts containing
   *  a keyword & a [mana cost]{@link https://mtg.gamepedia.com/Mana_cost} string or [reminder text]{@link https://mtg.gamepedia.com/Reminder_text}, or just 1 part being the keyword
   *
   * @param {[CardKeywordAbility , string , string]} parts
   */
  private parse(parts: [CardKeywordAbilityEnum, string?, string?]) {
    let manaCost: string;
    this.keyword = parts[0];

    if (parts.length === 3) {
      manaCost = parts[1];
      this.reminderText = parts[2];
    } else if (parts.length === 2) {
      parts[1].includes('(')
        ? (this.reminderText = parts[1])
        : (manaCost = parts[1]);
    }

    if (manaCost) {
      this.manaCost = new Mana(manaCost);
    }
  }
}
