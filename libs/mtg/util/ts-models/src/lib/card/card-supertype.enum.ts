export enum CardSupertype {
  Host = 'Host',
  Snow = 'Snow',
  Basic = 'Basic',
  Elite = 'Elite',
  World = 'World',
  Ongoing = 'Ongoing',
  Legendary = 'Legendary',
}
