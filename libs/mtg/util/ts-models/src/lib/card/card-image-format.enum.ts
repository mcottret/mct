export enum CardImageFormat {
  Png = 'PNG',
  Small = 'SMALL',
  Large = 'LARGE',
  Normal = 'NORMAL',
  ArtCrop = 'ART_CROP',
  Gatherer = 'GATHERER',
  BorderCrop = 'BORDER_CROP',
}
