import { Card, ColorSymbol, Mana } from '@mct/mtg/util/ts-models';

import { CardType } from './card-type.enum';
import { CardLayout } from './card-layout.enum';
import { CardRarity } from './card-rarity.enum';
import { CardSupertype } from './card-supertype.enum';
import { CardImageFormat } from './card-image-format.enum';

export interface CardInterface {
  name: string;
  typeLine: string;
  subtypes: string[];
  colorIdentity: ColorSymbol[];

  types: CardType[];
  supertypes: CardSupertype[];

  power?: string;
  loyalty?: string;
  toughness?: string;
  oracleText?: string;
  handModifier?: string;
  lifeModifier?: string;

  faces?: Card[];
  rarity?: CardRarity;
  layout?: CardLayout;
  colors?: ColorSymbol[];
  manaCost?: Mana;
  releaseDate?: Date;

  imagesUris?: {
    [format in CardImageFormat]: URL;
  };
}
