import { union, isEqual } from 'lodash-es';
import { product } from 'cartesian-product-generator';

import { subtract } from '@mct/shared/util/array';
import { ColorSymbol } from '@mct/mtg/util/ts-models';

export class Mana {
  /**
   * [Converted mana cost]{@link https://mtg.gamepedia.com/Mana_cost}. If this is not set, it will be considered infinite
   */
  cmc?: number;

  /**
   * The [colors]{@link https://mtg.gamepedia.com/Color} contained in this [Mana]{@link https://mtg.gamepedia.com/Mana}, if any
   */
  colors: ColorSymbol[] = [];

  /**
   * The possible ways to fulfill this [Mana's cost]{@link https://mtg.gamepedia.com/Mana_cost} colors. Consists of an
   *  array of [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} arrays, each representing an
   *  unique colors combination. If this is not set & the instance has at least one {@link colors}, every {@link Mana}
   *  instance with at least one matching colors will be considered fulfillable by this one
   *
   * See {@link canBeFulfilledWith}
   */
  waysToFulfill?: ColorSymbol[][];

  /**
   * @param manaCost A [mana cost](https://mtg.gamepedia.com/Mana_cost) string representation
   */
  constructor(manaCost?: string) {
    if (manaCost) {
      this.parse(manaCost);
    }
  }

  /**
   * Whether or not there is at least one [color]{@link https://mtg.gamepedia.com/Color} in this [Mana]{@link https://mtg.gamepedia.com/Mana}
   */
  isColored(): boolean {
    return !!this.colors.length;
  }

  /**
   * Whether or not this instance has the given [color]{@link https://mtg.gamepedia.com/Color}
   *
   * @param color
   */
  hasColor(color: ColorSymbol): boolean {
    return this.colors.includes(color);
  }

  /**
   * Returns a boolean indicating whether or not this [Mana's cost](https://mtg.gamepedia.com/Mana_cost) can be fulfilled
   *  by the given {@link Mana}, which requires:
   *
   *  - The given {@link Mana}'s {@link cmc} to be lesser than or equal to this instance's {@link cmc}
   *  - The given {@link Mana} [to be colored]{@link isColored} & have at least one common {@link colors} with this instance,
   *    if this instance [is colored]{@link isColored}
   *  - The given {@param mana} to have at least one matching {@link waysToFulfill} entry with this instance (this means that
   *    out of every [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} combinations in {@link waysToFulfill},
   *    there must be at least one [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} combination
   *    in {@param mana} which has at least the exact same number of [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols}
   *    of each color)
   *
   * @param mana
   *
   * @returns {boolean}
   */
  canBeFulfilledWith(mana: Mana): boolean {
    let canBeFulfilled = !Number.isFinite(mana.cmc) || this.cmc <= mana.cmc;

    if (canBeFulfilled && this.isColored()) {
      canBeFulfilled =
        this.colors.filter((color) => mana.hasColor(color)).length !== 0;

      if (canBeFulfilled && mana.waysToFulfill) {
        canBeFulfilled =
          this.waysToFulfill.findIndex(
            (wayToBeFulfilled) =>
              mana.waysToFulfill.findIndex(
                (wayToFulfill) =>
                  subtract(wayToBeFulfilled, wayToFulfill).length === 0
              ) !== -1
          ) !== -1;
      }
    }

    return canBeFulfilled;
  }

  /**
   * @param manaCost
   */
  private parse(manaCost: string) {
    const coloredManaSymbols = this.parseManaCost(manaCost);
    this.setWaysToFulfill(coloredManaSymbols);
  }

  /**
   * Parses a given [mana cost]{@link https://mtg.gamepedia.com/Mana_cost} string representation into this instance the
   *  following way:
   *
   *    - Extracts its [converted mana cost]{@link https://mtg.gamepedia.com/Mana_cost#Converted_mana_cost} into {@link cmc}
   *    - Extracts its individual color codes into {@link colors}, if any
   *
   * @param manaCost
   *
   * @returns {ColorSymbol[][]} every [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} arrays in the parsed
   *  [mana cost]{@link https://mtg.gamepedia.com/Mana_cost}, used to calculate its {@link waysToFulfill} (see {@link setWaysToFulfill})
   */
  private parseManaCost(manaCost: string): ColorSymbol[][] {
    this.cmc = 0;

    const coloredManaSymbols: ColorSymbol[][] = [];

    let manaSymbol: string | RegExpMatchArray;

    /**
     * Match every [symbol]{@link https://mtg.gamepedia.com/Numbers_and_symbols} in the [mana cost]{@link https://mtg.gamepedia.com/Mana_cost},
     *  each symbol is surrounded by curly braces
     */
    for (manaSymbol of manaCost.matchAll(/{(.*?)}/g)) {
      // We only want the matching groups (the symbols themselves, without the curly braces)
      manaSymbol = manaSymbol[1];

      /**
       * If it s a [colored mana symbol]{@link https://mtg.gamepedia.com/Numbers_and_symbols}:
       *  - Add 1 to {@link cmc}
       *  - Add each of its unique colors to {@link colors} if not already present
       *  - Add each symbol to {@link coloredManaSymbols}
       */
      if (isNaN(+manaSymbol)) {
        this.cmc++;

        /**
         * Split on '/' in case the symbol represents an [hybrid mana]{@link https://mtg.gamepedia.com/Hybrid_mana}
         */
        const colorsSymbols = manaSymbol.split('/') as ColorSymbol[];
        this.colors = union(this.colors, colorsSymbols);

        coloredManaSymbols.push(colorsSymbols);
      } else {
        /**
         * If it's a [colorless mana symbol]{@link https://mtg.gamepedia.com/Numbers_and_symbols}, add its numeric value to {@link cmc}
         *
         * See {@link https://mtg.gamepedia.com/Colorless}
         */
        this.cmc += +manaSymbol;
      }
    }

    return coloredManaSymbols;
  }

  /**
   * Set this instance's possible color payment combinations in its {@link waysToFulfill} array from an array of [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols}
   *  arrays (see below). It is equal to the cartesian product without duplicate of all the [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} arrays
   *  (eg: `[['G'], ['W', 'R'], ['W', 'R']]` would become `[['G', 'W', 'W'], ['G', 'W', 'R'], ['G', 'R', 'R']]`)
   *
   * @param coloredManaSymbols all the [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} arrays
   *  in the corresponding [mana cost]{@link https://mtg.gamepedia.com/Mana_cost}, where each symbol counts as an array
   *  entry (because some symbols like [hybrid mana symbols]{@link https://mtg.gamepedia.com/Hybrid_mana} may contain
   *  multiple [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols}). For example: `{G}{W/R}{W/R}`
   *  corresponds to the following [colored mana symbols]{@link https://mtg.gamepedia.com/Numbers_and_symbols} array: `[['G'], ['W', 'R'], ['W', 'R']]`)
   */
  setWaysToFulfill(coloredManaSymbols: ColorSymbol[][]) {
    if (coloredManaSymbols.length) {
      this.waysToFulfill = [];

      for (const wayToFulfill of product(...coloredManaSymbols)) {
        /**
         * Remove duplicates by sorting & comparing to existing {@link waysToFulfill}, because `['R', 'W']` is equivalent
         *  to `['W', 'R']`, for example
         */
        wayToFulfill.sort();

        if (
          !this.waysToFulfill.find((existingWayToFulfill) =>
            isEqual(existingWayToFulfill, wayToFulfill)
          )
        ) {
          this.waysToFulfill.push(wayToFulfill);
        }
      }
    }
  }
}
