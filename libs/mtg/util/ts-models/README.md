# MTG TS Models

MTG TS Models is a [Typescript](https://www.typescriptlang.org/) utility library exposing classes to map MTG entities (mana, cards, sets ...).
