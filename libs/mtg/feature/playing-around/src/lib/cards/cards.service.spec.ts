import { TestBed } from '@angular/core/testing';

import { CardsHttpService } from '@mct/mtg/data-access/scryfall-api-angular';

import { CardsService } from './cards.service';

describe('CardsService', () => {
  let service: CardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: CardsHttpService, useValue: {} }],
    });

    service = TestBed.inject(CardsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
