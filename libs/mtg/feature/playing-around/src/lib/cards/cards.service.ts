import { Injectable } from '@angular/core';

import { difference } from 'lodash-es';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import {
  catchError,
  distinct,
  filter,
  finalize,
  mergeMap,
  shareReplay,
  tap,
} from 'rxjs/operators';

import { Card, Set, SetCode } from '@mct/mtg/util/ts-models';
import { CardsHttpService } from '@mct/mtg/data-access/scryfall-api-angular';

@Injectable({
  providedIn: 'root',
})
export class CardsService {
  /** A {@link BehaviorSubject} emitting the sets currently loading cards whenever they change */
  setsLoadingCards$: BehaviorSubject<Set[]> = new BehaviorSubject<Set[]>([]);

  // The local cache of already loaded cards, indexed by their set's code
  private draftCardsBySetCode$: { [code in SetCode]?: Observable<Card> } = {};
  private instantSpeedDraftCardsBySetCode$: {
    [code in SetCode]?: Observable<Card>;
  } = {};

  constructor(private readonly cardsHttpService: CardsHttpService) {}

  private get setsLoadingCards() {
    return this.setsLoadingCards$.value;
  }

  /**
   * @param sets
   */
  private set setsLoadingCards(sets: Set[]) {
    this.setsLoadingCards$.next(sets);
  }

  /**
   * Computes the combined draft booster URI search params for the given sets.
   *
   * Since cards can be {@link https://mtg.fandom.com/wiki/Reprint reprinted} across several sets, the `unique` query
   *  parameter is set to `prints` so that no set could miss a reprinted card if displayed alone.
   *
   * The order is set to "newest released first", so that the latest print of every card will come first when filtering
   *  reprints across multiple sets.
   *
   * See {@link getAllDraftCardsBySets}
   *
   * @param sets
   */
  private static getSetsDraftBoosterUriSearchParams(
    sets: Set[]
  ): URLSearchParams {
    const setsSearchParams = sets
      .map((set) => set.draftBoosterSearchUri.searchParams)
      .reduce((accumulator, searchParams) => {
        accumulator.set(
          'q',
          `${accumulator.get('q')} or ${searchParams.get('q')}`
        );
        return accumulator;
      });

    setsSearchParams.set('unique', 'prints');
    setsSearchParams.set('order', 'released');

    return setsSearchParams;
  }

  /**
   * @param set
   */
  private removeSetLoadingCards(set: Set) {
    const setsLoadingCards = this.setsLoadingCards;
    const index = setsLoadingCards.indexOf(set);

    if (index !== -1) {
      setsLoadingCards.splice(index, 1);
      this.setsLoadingCards = setsLoadingCards;
    }
  }

  /**
   * Retrieves & caches an Observable emitting all the draft cards (found in boosters) from the Scryfall API for the given set
   *
   * @returns {Observable<Card>}
   */
  getAllDraftCardsBySet(set: Set): Observable<Card> {
    if (!this.draftCardsBySetCode$[set.code]) {
      const searchParams = set.draftBoosterSearchUri.searchParams;

      this.draftCardsBySetCode$[set.code] = of(null).pipe(
        tap(() => (this.setsLoadingCards = [...this.setsLoadingCards, set])),
        mergeMap(() =>
          this.cardsHttpService.getAllCardsByQuery(
            Object.fromEntries(searchParams.entries())
          )
        ),
        catchError(() => []),
        finalize(() => this.removeSetLoadingCards(set)),
        shareReplay({ refCount: true })
      );
    }

    return this.draftCardsBySetCode$[set.code];
  }

  /**
   * Retrieves an Observable emitting all the draft cards (found in boosters) from the Scryfall API for the given sets.
   *
   * Each set of cards not already in the {@link draftCardsBySetCode$ local cache} gets cached if needed.
   *
   * Since cards can be {@link https://mtg.fandom.com/wiki/Reprint reprinted} across several sets, only cards with
   *  distinct names are returned to avoid duplicates
   *
   * @param sets
   */
  getAllDraftCardsBySets(sets: Set[]): Observable<Card> {
    const cachedSetsCodes = Object.keys(this.draftCardsBySetCode$);
    const setsToCache = sets.filter(
      (set) => !cachedSetsCodes.includes(set.code)
    );

    if (setsToCache.length) {
      this.cacheSetsDraftCards(setsToCache);
    }

    return from(sets).pipe(
      mergeMap((set) => this.draftCardsBySetCode$[set.code]),
      distinct((card) => card.name)
    );
  }

  /**
   * Caches the draft cards for the given sets, storing an Observable of each set's draft cards into the {@link draftCardsBySetCode$ local cache}
   *
   * @param sets
   */
  private cacheSetsDraftCards(sets: Set[]) {
    const cardsToCache$ = this.getDraftCards$(sets);

    sets.forEach(
      (set) =>
        (this.draftCardsBySetCode$[set.code] = cardsToCache$.pipe(
          filter((card) => card.setCode === set.code),
          tap(() => this.removeSetLoadingCards(set)),
          shareReplay({ refCount: true })
        ))
    );
  }

  /**
   * Returns an Observable of all the draft cards for the given sets
   *
   * @param sets
   */
  private getDraftCards$(sets: Set[]): Observable<Card> {
    const searchParams = CardsService.getSetsDraftBoosterUriSearchParams(sets);

    return of(null).pipe(
      tap(() => (this.setsLoadingCards = [...this.setsLoadingCards, ...sets])),
      mergeMap(() =>
        this.cardsHttpService.getAllCardsByQuery(
          Object.fromEntries(searchParams.entries())
        )
      ),
      catchError(() => []),
      finalize(
        () => (this.setsLoadingCards = difference(this.setsLoadingCards, sets))
      ),
      shareReplay({ refCount: true })
    );
  }

  /**
   * Retrieves & caches an Observable emitting all the draft cards (found in boosters) playable at "[Instant speed]{@link https://mtg.gamepedia.com/Instant#Instant_speed}" from the Scryfall API for the given set
   *
   * @param set
   */
  getAllInstantSpeedDraftCardsBySet(set: Set) {
    if (!this.instantSpeedDraftCardsBySetCode$[set.code]) {
      this.instantSpeedDraftCardsBySetCode$[
        set.code
      ] = this.getAllDraftCardsBySet(set).pipe(
        filter((card) => card.hasInstantSpeed())
      );
    }

    return this.instantSpeedDraftCardsBySetCode$[set.code];
  }

  /**
   * Retrieves & caches an Observable emitting all the draft cards (found in boosters) playable at "[Instant speed]{@link https://mtg.gamepedia.com/Instant#Instant_speed}" from the Scryfall API for the given sets
   *
   * Each set of cards not already in the {@link instantSpeedDraftCardsBySetCode$ local cache} gets cached if needed.
   *
   * @param sets
   */
  getAllInstantSpeedDraftCardsBySets(sets: Set[]): Observable<Card> {
    const cachedSetsCodes = Object.keys(this.draftCardsBySetCode$);
    const setsToCache = sets.filter(
      (set) => !cachedSetsCodes.includes(set.code)
    );

    if (setsToCache.length) {
      this.cacheSetsDraftCards(setsToCache);

      setsToCache.forEach(set => this.instantSpeedDraftCardsBySetCode$[set.code] = this.draftCardsBySetCode$[set.code].pipe(filter(card => card.hasInstantSpeed())));
    }

    return from(sets).pipe(
      mergeMap((set) => this.instantSpeedDraftCardsBySetCode$[set.code]),
      distinct((card) => card.name)
    );
  }
}
