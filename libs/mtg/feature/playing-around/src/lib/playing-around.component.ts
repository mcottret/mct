import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { $enum } from 'ts-enum-util';
import { map as lodashMap } from 'lodash-es';
import {
  catchError,
  defaultIfEmpty,
  filter,
  first,
  map,
  scan,
  switchMap,
  take,
  throwIfEmpty,
  toArray,
} from 'rxjs/operators';
import {
  BehaviorSubject,
  combineLatest,
  EmptyError,
  Observable,
  ReplaySubject,
  Subject,
  throwError,
} from 'rxjs';

import {
  Card,
  ColorSymbol,
  Mana,
  Set as MtgSet,
  SetCode,
  CardKeywordAbilityEnum,
} from '@mct/mtg/util/ts-models';

import { SetsService } from './sets/sets.service';
import { CardsService } from './cards/cards.service';

@UntilDestroy()
@Component({
  selector: 'mct-mtg-feature-playing-around',
  templateUrl: './playing-around.component.html',
  styleUrls: ['./playing-around.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayingAroundComponent implements OnInit {
  draftSets$: Observable<MtgSet[]>;
  selectedSets$: ReplaySubject<MtgSet[]>;
  setsLoadingCards$: BehaviorSubject<MtgSet[]> = this.cardsService
    .setsLoadingCards$;
  maximumSetsSelected = 10;
  setsLoadingError$ = new Subject<boolean>();

  manaFilter$: ReplaySubject<Mana>;
  filteredCards$: Observable<Card[]>;

  constructor(
    private readonly router: Router,
    private readonly setsService: SetsService,
    private readonly cardsService: CardsService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  /**
   * Retrieves the first non-empty {@link https://en.wikipedia.org/wiki/List_of_Magic:_The_Gathering_sets#Base/core_set_editions sets codes} query parameter as an array, or []
   *
   * The accepted list of {@link https://en.wikipedia.org/wiki/List_of_Magic:_The_Gathering_sets#Base/core_set_editions sets codes} query parameters, in order of prevalence, is as follows:
   *  - sets
   *  - setsCodes
   *  - set
   *  - setCode
   *  - setCodes
   *
   * Merges values of multiple occurrences of the same query parameters keys & ignores whitespaces & case
   *
   * @param params
   * @private
   */
  private static getSetsCodesFromQueryParams(params: ParamMap): SetCode[] {
    let setsCodes: string[],
      i = 0;
    const keys = ['sets', 'setsCodes', 'set', 'setCode', 'setCodes'];

    do {
      setsCodes = params.getAll(keys[i]);
      ++i;
    } while (!setsCodes.length && i < keys.length);

    return setsCodes
      .join(',')
      .toLowerCase()
      .replace(/\s+/g, '')
      .split(',') as SetCode[];
  }

  /**
   * Retrieves the first positive integer {@link https://mtg.fandom.com/wiki/Mana_cost cmc} query parameter, or 0
   *
   * The accepted list of {@link https://mtg.fandom.com/wiki/Mana_cost cmc} query parameters, in order of prevalence, is as follows:
   *  - cmc
   *  - cmcMax
   *  - maxCmc
   *
   * @param params
   * @private
   */
  private static getCmcFromQueryParams(params: ParamMap): number {
    let cmc: number,
      i = 0;
    const keys = ['cmc', 'cmcMax', 'maxCmc'];

    do {
      cmc = Math.min(parseInt(params.get(keys[i]), 10), 7);
      ++i;
    } while (!(cmc && Number.isInteger(cmc) && cmc >= 0) && i < keys.length);

    return cmc || 0;
  }

  ngOnInit() {
    this.draftSets$ = this.setsService.draftSets$.pipe(toArray());

    this.initManaFilter$();
    this.initSelectedSets$();
    this.initFilteredCards$();
    this.rewriteQueryParameters();
  }

  /**
   * Updates the `setsCodes` query parameter whenever the selected sets change
   *
   * @param sets
   */
  onSetsChanged(sets: MtgSet[]) {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        sets: lodashMap(sets, 'code').join(','),
      },
    });
  }

  /**
   * Updates the `cmc` & `colors` query parameters whenever the selected mana changes
   *
   * @param mana
   */
  onManaChanged(mana: Mana) {
    const queryParams: Params = {
      ...this.activatedRoute.snapshot.queryParams,
      cmc: mana.cmc ?? 7,
    };

    if (mana.colors.length) {
      queryParams.colors = mana.colors.join(',');
    } else {
      delete queryParams.colors;
    }

    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams,
    });
  }

  /**
   * @param setsLoadingCards
   */
  getSetsLoadingCardsMessage(setsLoadingCards: MtgSet[]) {
    let message = '';

    if (setsLoadingCards.length) {
      message += `Loading cards for ${setsLoadingCards[0].name}`;

      if (setsLoadingCards.length > 1) {
        message += ` and ${(setsLoadingCards.length - 1).toString()} more`;
      }

      message += ' ...';
    }

    return message;
  }

  /**
   * Initializes the {@link manaFilter$ mana filter} {@link ReplaySubject subject}, which emits whenever the query
   *  parameters change.
   *
   * See {@link getManaFilterFromQueryParams}
   */
  private initManaFilter$() {
    this.manaFilter$ = new ReplaySubject<Mana>(1);

    this.activatedRoute.queryParamMap
      .pipe(
        untilDestroyed(this),
        map((params) => this.getManaFilterFromQueryParams(params))
      )
      .subscribe(this.manaFilter$);
  }

  /**
   * Retrieves a {@link Mana mana filter} corresponding to the first valid {@link https://mtg.gamepedia.com/Color colors symbols} & {@link https://mtg.fandom.com/wiki/Mana_cost cmc} query parameters, falls back to a
   *  {@link https://mtg.fandom.com/wiki/Zero_Cost_Cards zero cost mana} otherwise.
   *
   * See:
   *  - {@link getCmcFromQueryParams}
   *  - {@link getColorsFromQueryParams}
   *
   * @param params
   */
  private getManaFilterFromQueryParams(params: ParamMap): Mana {
    const manaFilter = new Mana();

    manaFilter.cmc = PlayingAroundComponent.getCmcFromQueryParams(params);
    manaFilter.colors = this.getColorsFromQueryParams(params);

    return manaFilter;
  }

  /**
   * Retrieves the first non-empty valid {@link https://mtg.gamepedia.com/Color colors symbols} query parameter as an array, or []
   *
   * The accepted list of {@link https://mtg.gamepedia.com/Color colors symbols} query parameters, in order of prevalence, is as follows:
   *  - colors
   *  - colours
   *  - colorSymbols
   *  - colorsSymbols
   *  - colourSymbols
   *  - coloursSymbols
   *  - selectedColors
   *  - selectedColours
   *  - color
   *  - colour
   *
   * Merges values of multiple occurrences of the same query parameters keys & ignores whitespaces, case & invalid values
   *
   * @param params
   * @private
   */
  private getColorsFromQueryParams(params: ParamMap): ColorSymbol[] {
    let selectedColors: string[],
      i = 0;
    const keys = [
      'colors',
      'colours',
      'colorSymbols',
      'colorsSymbols',
      'colourSymbols',
      'coloursSymbols',
      'selectedColors',
      'selectedColours',
      'color',
      'colour',
    ];

    do {
      selectedColors = [
        ...new Set(
          params
            .getAll(keys[i])
            .join(',')
            .toUpperCase()
            .replace(/\s+/g, '')
            .split(',')
            .filter((colorSymbol) => $enum(ColorSymbol).isValue(colorSymbol))
        ),
      ];

      ++i;
    } while (!selectedColors.length && i < keys.length);

    return selectedColors as ColorSymbol[];
  }

  /**
   * Initializes the {@link selectedSets$ selected sets} {@link ReplaySubject subject}, which emits whenever the query
   *  parameters change.
   *
   * See {@link getSets$FromQueryParams}
   */
  private initSelectedSets$() {
    this.selectedSets$ = new ReplaySubject<MtgSet[]>(1);

    this.activatedRoute.queryParamMap
      .pipe(
        untilDestroyed(this),
        switchMap((params) => this.getSets$FromQueryParams(params))
      )
      .subscribe(this.selectedSets$);
  }

  /**
   * Retrieves an Observable emitting sets corresponding to the first non-empty valid {@link https://en.wikipedia.org/wiki/List_of_Magic:_The_Gathering_sets#Base/core_set_editions sets codes} query parameter
   *  as array, falls back to the latest released set otherwise.
   *
   * Ignores invalid {@link https://en.wikipedia.org/wiki/List_of_Magic:_The_Gathering_sets#Base/core_set_editions sets codes}, but only after
   *  establishing prevalence, meaning that a prevalent query parameter containing only invalid values will still fall back
   *  to the latest released set, even if a less prevalent one contains a valid set code
   *
   * See {@link getSetsCodesFromQueryParams}
   *
   * @param params
   */
  private getSets$FromQueryParams(params: ParamMap): Observable<MtgSet[]> {
    const setsCodes = PlayingAroundComponent.getSetsCodesFromQueryParams(
      params
    );
    const sets$ = this.setsService
      .getDraftSetsByCodes(setsCodes)
      .pipe(take(this.maximumSetsSelected));

    return sets$.pipe(
      /**
       * Manual implementation of the `switchIfEmpty` operator
       *
       * See {@link https://github.com/ReactiveX/rxjs/issues/3714}
       */
      throwIfEmpty(),
      catchError((error) => {
        let result;

        if (error instanceof EmptyError) {
          result = this.setsService.getLatestReleasedDraftSet();
        } else {
          this.setsLoadingError$.next(true);
          result = throwError(error);
        }

        return result as Observable<MtgSet>;
      }),
      toArray()
    );
  }

  /**
   * Updates the {@link filteredCards$ filtered cards list} whenever the {@link selectedSets$ selected sets} or {@link manaFilter$ mana filter}
   *  changes
   */
  private initFilteredCards$() {
    this.filteredCards$ = combineLatest([
      this.selectedSets$,
      this.manaFilter$,
    ]).pipe(
      switchMap(([sets, manaFilter]) =>
        this.cardsService.getAllInstantSpeedDraftCardsBySets(sets).pipe(
          filter(card => card.isPlayable(manaFilter, [
                  CardKeywordAbilityEnum.Cycling,
                  CardKeywordAbilityEnum.Foretell,
                  CardKeywordAbilityEnum.Mutate,
                ])
              ),
          scan((filteredCards, card) => [...filteredCards, card], []),
          defaultIfEmpty([])
        )
      )
    );
  }

  /**
   * Rewrites the URL's query parameters:
   *
   *  - Replaces invalid values by their default
   *  - Removes duplicates keys
   *  - Removes unrecognized parameters
   *
   * See:
   *  - {@link initManaFilter$}
   *  - {@link initSelectedSets$}
   *
   * @private
   */
  private rewriteQueryParameters() {
    combineLatest([this.selectedSets$, this.manaFilter$])
      .pipe(untilDestroyed(this), first())
      .subscribe(([sets, manaFilter]) => {
        this.router.navigate([], {
          relativeTo: this.activatedRoute,
          queryParams: {
            cmc: manaFilter.cmc,
            sets: lodashMap(sets, 'code').join(','),
            colors: manaFilter.colors.join(',') || null,
          },
        });
      });
  }
}
