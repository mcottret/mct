import { TestBed, waitForAsync } from '@angular/core/testing';

import { PlayingAroundModule } from './playing-around.module';

describe('PlayingAroundModule', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [PlayingAroundModule],
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(PlayingAroundModule).toBeDefined();
  });
});
