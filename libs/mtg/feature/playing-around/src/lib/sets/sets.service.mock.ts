import { of } from 'rxjs';

import { SetsService } from './sets.service';

const setsServiceSpy = {
  draftSets$: of(),
  getLatestReleasedDraftSet: jest.fn(() => of()),
};

export const SetsServiceMock = {
  provide: SetsService,
  useValue: setsServiceSpy,
};
