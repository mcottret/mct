import { TestBed } from '@angular/core/testing';

import { SetsHttpService } from '@mct/mtg/data-access/scryfall-api-angular';

import { SetsService } from './sets.service';

describe('SetsService', () => {
  let service: SetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: SetsHttpService, useValue: {} }],
    });

    service = TestBed.inject(SetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
