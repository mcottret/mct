import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { filter, find, reduce, shareReplay } from 'rxjs/operators';

import { Set, SetCode } from '@mct/mtg/util/ts-models';
import { SetsHttpService } from '@mct/mtg/data-access/scryfall-api-angular';

@Injectable({
  providedIn: 'root',
})
export class SetsService {
  private _allSets$: Observable<Set>;
  private _draftSets$: Observable<Set>;

  constructor(private readonly setsHttpService: SetsHttpService) {}

  private get allSets$() {
    if (!this._allSets$) {
      this._allSets$ = this.setsHttpService
        .getAll()
        .pipe(shareReplay({ refCount: true }));
    }

    return this._allSets$;
  }

  get draftSets$(): Observable<Set> {
    if (!this._draftSets$) {
      this._draftSets$ = this.allSets$.pipe(filter((set) => set.isDraftable()));
    }

    return this._draftSets$;
  }

  /**
   * Retrieves an Observable of the draft Set with the given code
   *
   * @param setCode
   */
  getDraftSetByCode(setCode: SetCode): Observable<Set> {
    return this.draftSets$.pipe(find((set) => set.code === setCode));
  }

  /**
   * Retrieves an Observable emitting all the draft Sets for the given codes
   *
   * @param setsCodes
   */
  getDraftSetsByCodes(setsCodes: SetCode[]): Observable<Set> {
    return this.draftSets$.pipe(filter((set) => setsCodes.includes(set.code)));
  }

  /**
   * Retrieves an Observable of the latest released draft Set
   *
   * Note: the API currently doesn't allow enough `GET /sets` requests filters to directly retrieve the latest
   *  released draft Set. So the filtering is performed on the whole sets list here instead
   *
   * @returns {Observable<Set>}
   */
  getLatestReleasedDraftSet(): Observable<Set> {
    const reducer = (accumulator, currentValue) =>
      currentValue.isReleased() &&
      (accumulator
        ? currentValue.wasReleasedAfter(accumulator.getReleaseTimestamp())
        : true)
        ? currentValue
        : accumulator;

    return this.draftSets$.pipe(reduce(reducer, null));
  }
}
