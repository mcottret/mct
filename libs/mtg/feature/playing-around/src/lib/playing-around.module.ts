import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SetModule } from '@mct/mtg/ui/set';
import { ManaModule } from '@mct/mtg/ui/mana';
import { CardModule } from '@mct/mtg/ui/card';
import { ScryfallApiModule } from '@mct/mtg/data-access/scryfall-api-angular';

import { routes } from './routes';
import { PlayingAroundComponent } from './playing-around.component';

@NgModule({
  imports: [
    SetModule,
    ManaModule,
    CardModule,
    CommonModule,
    ScryfallApiModule,
    RouterModule.forChild(routes),
  ],
  declarations: [PlayingAroundComponent],
})
export class PlayingAroundModule {}
