import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardComponentMock } from '@mct/mtg/ui/card';
import { SetSelectorComponentMock } from '@mct/mtg/ui/set';
import { ManaSelectorComponentMock } from '@mct/mtg/ui/mana';

import { CardsService } from './cards/cards.service';
import { SetsServiceMock } from './sets/sets.service.mock';
import { PlayingAroundComponent } from './playing-around.component';

describe('PlayingAroundComponent', () => {
  let component: PlayingAroundComponent;
  let fixture: ComponentFixture<PlayingAroundComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        providers: [SetsServiceMock, { provide: CardsService, useValue: {} }],
        declarations: [
          CardComponentMock,
          PlayingAroundComponent,
          SetSelectorComponentMock,
          ManaSelectorComponentMock,
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayingAroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
