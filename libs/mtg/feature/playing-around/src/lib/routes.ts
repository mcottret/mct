import { Routes } from '@angular/router';

import { PlayingAroundComponent } from './playing-around.component';

export const routes: Routes = [{ path: '', component: PlayingAroundComponent }];
