# [MTG: Playing Around](https://mtg.gamepedia.com/List_of_Magic_slang#P) Feature

[MTG: Playing Around](https://mtg.gamepedia.com/List_of_Magic_slang#P) Feature is an [Angular](https://angular.io/) feature library exposing the `PlayingAroundComponent`.

Based on the term ["Playing Around"](https://mtg.gamepedia.com/List_of_Magic_slang#P) from [Magic: The Gathering](https://magic.wizards.com/en) card game, this component offers an UI allowing to calculate possible card plays during a game from a given set of parameters (mana, set, format ...).

## Dependencies

_Modules:_

- [`SetModule`](/libs/mtg/ui/set/README.md)
- [`ManaModule`](/libs/mtg/ui/mana/README.md)
- [`CardModule`](/libs/mtg/ui/card/README.md)   
- [`ScryfallApiModule`](/libs/mtg/data-access/scryfall-api-angular/README.md)

_Styles:_

- [Shared Styles](/libs/shared/util/styles/README.md)
