export enum CardLayout {
  Flip = 'flip',
  Plane = 'plane',
  Token = 'token',
  Split = 'split',
  Scheme = 'scheme',
  Normal = 'normal',
  Leveler = 'leveler',
  Vanguard = 'vanguard',
  Aftermath = 'aftermath',
  Phenomenon = 'phenomenon',
  DoubleFaced = 'double-faced',
}
