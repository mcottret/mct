import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CardsHttpService } from './cards-http.service';

describe('CardsHttpService', () => {
  let service: CardsHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(CardsHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
