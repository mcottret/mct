import {$enum} from 'ts-enum-util';

import {CardImageFormat, CardInterface, CardLayout, CardRarity, Mana,} from '@mct/mtg/util/ts-models';
import {Mappings} from '@mct/shared/util/object';

import {CardLayout as MtgApiCardLayout} from './card-layout.enum';
import {CardRarity as MtgApiCardRarity} from './card-rarity.enum';
import {CardInterface as MtgApiCardInterface} from './card.interface';

export const CardInterfaceMappings: Mappings<
  MtgApiCardInterface,
  CardInterface
> = {
  text: 'oracleText',
  type: 'typeLine',
  hand: 'handModifier',
  life: 'lifeModifier',

  releaseDate: {
    value: (card) => new Date(`${card.releaseDate}T00:00:00Z`),
    default: new Date(0),
  },

  manaCost: (card) => new Mana(card.manaCost),

  layout: (card) =>
    $enum(CardLayout).getValueOrDefault(
      $enum(MtgApiCardLayout).getKeyOrDefault(card.layout),
      card.layout
    ),

  rarity: (card) =>
    $enum(CardRarity).getValueOrDefault(
      $enum(MtgApiCardRarity).getKeyOrDefault(card.rarity),
      card.rarity
    ),

  imageUrl: {
    key: 'imagesUris',
    value: (card) => ({ [CardImageFormat.Gatherer]: new URL(card.imageUrl) }),
  },
};
