import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, range } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { setFromMappings } from '@mct/shared/util/object';
import { PaginatedResponse } from '@mct/shared/util/http-response-types';
import { Card, CardInterface } from '@mct/mtg/util/ts-models';

import { MtgApi } from '../mtg-api.constant';
import { MtgApiService } from '../mtg-api.service';
import { CardInterfaceMappings } from './card-interface-mappings.constant';
import { CardsResponseInterface } from './cards-response.interface';
import { CardInterface as MtgApiCardInterface } from './card.interface';

@Injectable({
  providedIn: 'root',
})
export class CardsHttpService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly mtgApiService: MtgApiService
  ) {}

  /**
   * Retrieves the total number of cards from the MTG API for the given parameters
   *
   * See {@link https://docs.magicthegathering.io/#api_v1cards_list}
   *
   * @returns {Observable<number>}
   */
  getTotalCount(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<number> {
    return this.httpClient
      .head(this.getEndpoint(), { params, observe: 'response' })
      .pipe(
        map((response) => parseInt(response.headers.get('total-count'), 10))
      );
  }

  /**
   * Retrieves an Observable emitting all the cards, parsed into {@link Card}, from the MTG API for the given parameters
   *
   * Since the `GET /cards` requests returns a maximum of [MtgApi.maximumCardsPageSize]{@link MtgApi#maximumCardsPageSize} cards, a `HEAD /cards` request
   *  is first performed to retrieve the total count of cards available, allowing to simultaneously launch the
   *  appropriate number of requests to retrieve all the cards
   *
   * Note: the `pageSize` query parameter will always default to [MtgApi.maximumCardsPageSize]{@link MtgApi#maximumCardsPageSize}, regardless of the value
   *  possibly provided in `params`
   *
   * See {@link https://docs.magicthegathering.io/#api_v1cards_list}
   *
   * @returns {Observable<Card>}
   */
  getAll(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<Card> {
    return this.getTotalCount(params).pipe(
      mergeMap((totalCount) =>
        range(1, Math.ceil(totalCount / MtgApi.maximumCardsPageSize))
      ),
      mergeMap((page) =>
        this.getCardsByPage(
          Object.assign(params, { page, pageSize: MtgApi.maximumCardsPageSize })
        )
      )
    );
  }

  /**
   * Retrieves a card page from the MTG API for the given parameters, parses it into a [PaginatedResponse<Card>]{@link PaginatedResponse}
   *
   * See {@link https://docs.magicthegathering.io/#api_v1cards_list}
   *
   * @returns {Observable<PaginatedResponse<Card>>}
   */
  getPage(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<PaginatedResponse<Card>> {
    return this.httpClient
      .get<CardsResponseInterface>(this.getEndpoint(), {
        params,
        observe: 'response',
      })
      .pipe(
        map((response) => ({
          data: response.body.cards.map(
            (card) =>
              new Card(
                setFromMappings<MtgApiCardInterface, CardInterface>(
                  card,
                  CardInterfaceMappings
                )
              )
          ),
          count: parseInt(response.headers.get('count'), 10),
          pageSize: parseInt(response.headers.get('page-size'), 10),
          totalCount: parseInt(response.headers.get('total-count'), 10),
        }))
      );
  }

  /**
   * Retrieves an Observable emitting all the cards of a given page, parsed into {@link Card}, for the given parameters from the MTG API
   *
   * See {@link https://docs.magicthegathering.io/#api_v1cards_list}
   *
   * @returns {Observable<Card>}
   */
  getCardsByPage(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<Card> {
    return this.httpClient
      .get<CardsResponseInterface>(this.getEndpoint(), { params })
      .pipe(
        mergeMap((response) =>
          response.cards.map(
            (card) =>
              new Card(
                setFromMappings<MtgApiCardInterface, CardInterface>(
                  card,
                  CardInterfaceMappings
                )
              )
          )
        )
      );
  }

  /**
   * Retrieves MTG API endpoint for the cards resource
   *
   * @returns {string}
   */
  private getEndpoint(): string {
    return `${this.mtgApiService.getEndpoint()}/cards`;
  }
}
