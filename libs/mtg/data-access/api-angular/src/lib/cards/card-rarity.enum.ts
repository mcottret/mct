export enum CardRarity {
  Rare = 'Rare',
  Common = 'Common',
  Special = 'Special',
  Uncommon = 'Uncommon',
  BasicLand = 'Basic Land',
  MythicRare = 'Mythic Rare',
}
