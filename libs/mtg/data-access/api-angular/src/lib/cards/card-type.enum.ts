export enum CardType {
  Land = 'Land',
  Instant = 'Instant',
  Sorcery = 'Sorcery',
  Artifact = 'Artifact',
  Creature = 'Creature',
  Enchantment = 'Enchantment',
  Planeswalker = 'Planeswalker',
}
