import { ColorSymbol } from '@mct/mtg/util/ts-models';

import { CardType } from './card-type.enum';
import { CardBorder } from './card-border.enum';
import { CardLayout } from './card-layout.enum';
import { CardRarity } from './card-rarity.enum';
import { CardSupertype } from './card-supertype.enum';
import { CardForeignName } from './card-foreign-name.interface';
import { CardLegalityInterface } from './legality/card-legality.interface';

export interface CardInterface {
  id: string;
  cmc: number;
  set: string;
  name: string;
  text: string;
  type: string;
  names: string[];
  flavor: string;
  source: string;
  artist: string;
  number: string;
  colors: ColorSymbol[];
  starter: boolean;
  setName: string;
  reserved: boolean;
  legality: string;
  language: string;
  subtypes: string[];
  manaCost: string;
  imageUrl: string;
  watermark: string;
  printings: string[];
  gameFormat: string;
  variations: number[];
  timeshifted: boolean;
  colorIdentity: ColorSymbol[];

  types: CardType[];
  rarity: CardRarity;
  border: CardBorder;
  layout: CardLayout;
  supertypes: CardSupertype[];
  legalities: CardLegalityInterface[];

  rulings: {
    date: string /** YYYY-MM-DD {@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time UTC} **/;
    text: string;
  }[];

  hand?: string;
  life?: string;
  power?: string;
  loyalty?: number;
  toughness?: string;
  releaseDate?: string /** YYYY-MM-DD {@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time UTC} **/;
  multiverseid?: number;
  originalText?: string;
  originalType?: string;

  foreignNames?: CardForeignName[];
}
