import { CardInterface } from './card.interface';

export interface CardsResponseInterface {
  cards: CardInterface[];
}
