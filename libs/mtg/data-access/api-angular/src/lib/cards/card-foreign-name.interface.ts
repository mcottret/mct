export interface CardForeignName {
  name: string;
  language: string;
  multiverseid: number;
}
