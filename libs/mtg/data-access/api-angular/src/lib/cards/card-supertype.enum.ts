export enum CardSupertype {
  Snow = 'Snow',
  Basic = 'Basic',
  World = 'World',
  Ongoing = 'Ongoing',
  Legendary = 'Legendary',
}
