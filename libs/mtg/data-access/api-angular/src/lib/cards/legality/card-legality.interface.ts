import { CardLegality } from './card-legality.enum';

export interface CardLegalityInterface {
  format: string;
  legality: CardLegality;
}
