export enum CardLegality {
  Legal = 'Legal',
  Banned = 'Banned',
  Restricted = 'Restricted',
}
