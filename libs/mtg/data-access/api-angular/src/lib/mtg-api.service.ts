import { Injectable } from '@angular/core';

import { MtgApi } from './mtg-api.constant';

@Injectable({
  providedIn: 'root',
})
export class MtgApiService {
  constructor() {}

  /**
   * Retrieves the base MTG API endpoint
   *
   * @returns {string}
   */
  getEndpoint(): string {
    return `${MtgApi.baseUrl}/${MtgApi.version}`;
  }
}
