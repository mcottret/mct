import { $enum } from 'ts-enum-util';

import { Mappings } from '@mct/shared/util/object';
import { SetInterface, SetType } from '@mct/mtg/util/ts-models';

import { SetType as MtgApiSetType } from './set-type.enum';
import { SetInterface as MtgApiSetInterface } from './set.interface';

export const SetInterfaceMappings: Mappings<
  MtgApiSetInterface,
  SetInterface
> = {
  releaseDate: {
    value: (set) => new Date(`${set.releaseDate}T00:00:00Z`),
    default: new Date(0),
  },

  type: (set) =>
    $enum(SetType).getValueOrDefault(
      $enum(MtgApiSetType).getKeyOrDefault(set.type),
      set.type
    ),
};
