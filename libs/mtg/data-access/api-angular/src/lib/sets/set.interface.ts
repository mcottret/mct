import { SetType } from './set-type.enum';
import { CardBorder } from '../cards/card-border.enum';

export interface SetInterface {
  name: string;
  code: string;
  block: string;
  booster: string[];
  onlineOnly: boolean;
  releaseDate: string /** YYYY-MM-DD {@link https://en.wikipedia.org/wiki/Coordinated_Universal_Time UTC} **/;

  type: SetType;
  border: CardBorder;

  oldCode?: string;
  gathererCode?: string;
  magicCardsInfoCode?: string;
}
