import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { setFromMappings } from '@mct/shared/util/object';
import { Set, SetInterface } from '@mct/mtg/util/ts-models';

import { MtgApiService } from '../mtg-api.service';
import { SetInterfaceMappings } from './set-interface-mappings.constant';
import { SetsResponseInterface } from './sets-response.interface';
import { SetInterface as MtgApiSetInterface } from './set.interface';

@Injectable({
  providedIn: 'root',
})
export class SetsHttpService {
  constructor(
    private readonly mtgApiService: MtgApiService,
    private readonly httpClient: HttpClient
  ) {}

  /**
   * Retrieves an Observable emitting all the sets from the MTG API, parsed into Set
   *
   * @returns {Observable<Set>}
   */
  getAll(): Observable<Set> {
    return this.httpClient
      .get<SetsResponseInterface>(this.getEndpoint())
      .pipe(
        mergeMap((response) =>
          response.sets.map(
            (set) =>
              new Set(
                setFromMappings<MtgApiSetInterface, SetInterface>(
                  set,
                  SetInterfaceMappings
                )
              )
          )
        )
      );
  }

  /**
   * Retrieves MTG API endpoint for the sets resource
   *
   * @returns {string}
   */
  private getEndpoint(): string {
    return `${this.mtgApiService.getEndpoint()}/sets`;
  }
}
