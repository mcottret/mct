import { SetInterface } from './set.interface';

export interface SetsResponseInterface {
  sets: SetInterface[];
}
