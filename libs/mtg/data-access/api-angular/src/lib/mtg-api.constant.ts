export const MtgApi = {
  baseUrl: 'https://api.magicthegathering.io',
  version: 'v1',
  maximumCardsPageSize: 100,
};
