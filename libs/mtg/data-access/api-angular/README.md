# [MTG API](https://magicthegathering.io/) [Angular](https://angular.io/)

[MTG API](https://magicthegathering.io/) [Angular](https://angular.io/) is an [Angular](https://angular.io/) wrapper for the [MTG API](https://magicthegathering.io/)

## Dependencies

_Modules:_

- [`HttpClientModule`](https://angular.io/guide/http)

## Usage

To use [MTG API](https://magicthegathering.io/) [Angular](https://angular.io/), import the `MtgApiModule` from `@mct/mtg/data-access/api-angular` in your requiring module as in the example below:

```typescript
import { HttpClientModule } from '@angular/common/http';

import { MtgApiModule } from '@mct/mtg/data-access/api-angular';

@NgModule({
  imports: [HttpClientModule, MtgApiModule],
})
export class AppModule {}
```

You may also need to import `HttpClientModule` from your application module if it does not already
