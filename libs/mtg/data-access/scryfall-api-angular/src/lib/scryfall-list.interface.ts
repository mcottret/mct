export interface ScryfallList<T> {
  data: T[];
  has_more: boolean;

  warnings?: string[];
  next_page?: string; // URI
  total_cards?: number;
}
