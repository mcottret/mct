export enum CardCurrency {
  Tix = 'tix',
  Eur = 'eur',
  Usd = 'usd',
  UsdFoil = 'usd_foil',
}
