export enum CardRarity {
  Rare = 'rare',
  Mythic = 'mythic',
  Common = 'common',
  Uncommon = 'uncommon',
}
