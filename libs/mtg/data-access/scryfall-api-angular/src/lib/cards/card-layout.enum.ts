export enum CardLayout {
  Saga = 'saga',
  Meld = 'meld',
  Host = 'host',
  Flip = 'flip',
  Split = 'split',
  Token = 'token',
  Normal = 'normal',
  Planar = 'planar',
  Emblem = 'emblem',
  Scheme = 'scheme',
  Augment = 'augment',
  Leveler = 'leveler',
  Vanguard = 'vanguard',
  ArtSeries = 'art_series',
  Transform = 'transform',
  Adventure = 'adventure',
  DoubleFaced = 'modal_dfc',
  DoubleFacedToken = 'double_faced_token',
}
