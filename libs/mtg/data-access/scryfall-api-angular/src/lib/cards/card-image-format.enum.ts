export enum CardImageFormat {
  Png = 'png',
  Small = 'small',
  Large = 'large',
  Normal = 'normal',
  ArtCrop = 'art_crop',
  BorderCrop = 'border_crop',
}
