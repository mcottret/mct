export enum CardBorderColor {
  Gold = 'gold',
  White = 'white',
  Black = 'black',
  Silver = 'silver',
  Borderless = 'borderless',
}
