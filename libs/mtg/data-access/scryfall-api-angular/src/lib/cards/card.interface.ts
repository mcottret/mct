import { ColorSymbol } from '@mct/mtg/util/ts-models';

import { CardFace } from './card-face.interface';
import { CardFrame } from './frame/card-frame.enum';
import { CardLayout } from './card-layout.enum';
import { CardRarity } from './card-rarity.enum';
import { RelatedCard } from './related/related-card.interface';
import { ScryfallGame } from '../scryfall-game.enum';
import { CardLegality } from './card-legality.enum';
import { CardCurrency } from './card-currency.enum';
import { CardImageFormat } from './card-image-format.enum';
import { CardBorderColor } from './card-border-color.enum';
import { CardFrameEffect } from './frame/card-frame-effect.enum';

export interface CardInterface {
  id: string; // UUID
  set: string;
  cmc: number;
  uri: string; // URI
  name: string;
  lang: string; // https://scryfall.com/docs/api/languages
  foil: boolean;
  promo: boolean;
  object: 'card';
  nonfoil: boolean;
  digital: boolean;
  booster: boolean;
  reprint: boolean;
  set_uri: string; // URI
  textless: boolean;
  set_name: string;
  set_type: string;
  reserved: boolean;
  full_art: boolean;
  type_line: string;
  variation: boolean;
  oversized: boolean;
  oracle_id: string; // UUID
  released_at: string; // YYYY-MM-DD
  rulings_uri: string; // URI
  scryfall_uri: string; // URI
  card_back_id: string; // UUID
  highres_image: boolean;
  set_search_uri: string; // URI
  story_spotlight: boolean;
  collector_number: string;
  scryfall_set_uri: string; // URI
  prints_search_uri: string; // URI

  games: ScryfallGame[];
  frame: CardFrame;
  layout: CardLayout;
  rarity: CardRarity;
  border_color: CardBorderColor;
  frame_effects: CardFrameEffect[];
  color_identity: ColorSymbol[]; // https://scryfall.com/docs/api/colors

  prices: {
    [currency in CardCurrency]: string; // Price
  };

  legalities: {
    [format: string]: CardLegality;
  };

  related_uris: {
    [platform: string]: string; // URI
  };

  purchase_uris: {
    [marketplace: string]: string; // URI
  };

  power?: string;
  artist?: string;
  loyalty?: string;
  mtgo_id?: number;
  arena_id?: number;
  watermark?: string;
  toughness?: string;
  mana_cost?: string;
  flavor_text?: string;
  flavor_name?: string;
  oracle_text?: string;
  promo_types?: string[];
  edhrec_rank?: number;
  variation_of?: string; // UUID
  printed_text?: string;
  printed_name?: string;
  mtgo_foil_id?: number;
  tcgplayer_id?: number;
  life_modifier?: string;
  hand_modifier?: string;
  multiverse_ids?: number[];
  illustration_id?: string; // UUID
  printed_type_line?: string;

  colors?: ColorSymbol[]; // https://scryfall.com/docs/api/colors
  all_parts?: RelatedCard[];
  card_faces?: CardFace[];
  color_indicator?: ColorSymbol[]; // https://scryfall.com/docs/api/colors

  preview?: {
    source: string;
    source_uri: string; // URI
    previewed_at: string; // YYYY-MM-DD
  };

  image_uris?: {
    [format in CardImageFormat]: string; // URI
  };
}
