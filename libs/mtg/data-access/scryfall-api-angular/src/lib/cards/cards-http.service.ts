import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { map, mergeAll, mergeMap } from 'rxjs/operators';
import { iif, merge, Observable, of, range } from 'rxjs';

import { setFromMappings } from '@mct/shared/util/object';
import { Card, CardInterface } from '@mct/mtg/util/ts-models';

import { ScryfallApi } from '../scryfall-api.constant';
import { ScryfallList } from '../scryfall-list.interface';
import { ScryfallApiService } from '../scryfall-api.service';
import { CardInterfaceMappings } from './card-interface-mappings.constant';
import { CardInterface as ScryfallCardInterface } from './card.interface';

@Injectable({
  providedIn: 'root',
})
export class CardsHttpService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly scryfallApiService: ScryfallApiService
  ) {}

  /**
   * Retrieves an Observable emitting all the cards, parsed into {@link Card}, from the Scryfall API for the given parameters
   *
   * Since the `GET /cards/search` requests returns a maximum of [ScryfallApi.maximumCardsPageSize]{@link ScryfallApi#maximumCardsPageSize} cards, the first
   *  page is requested first to retrieve the total count of cards available, allowing to simultaneously launch the
   *  appropriate number of subsequent requests to retrieve the remaining cards if there's more than one page
   *
   * See {@link https://scryfall.com/docs/api/cards/search}
   *
   * @returns {Observable<Card>}
   */
  getAllCardsByQuery(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<Card> {
    return this.getPageByQuery(Object.assign(params, { page: 1 })).pipe(
      mergeMap((firstPage) => {
        const pageCount = Math.ceil(
          firstPage.total_cards / ScryfallApi.maximumCardsPageSize
        );
        const firstPageCards$ = of(firstPage.data).pipe(mergeAll());
        const remainingPagesCards$ = range(2, pageCount - 1).pipe(
          mergeMap((page) =>
            this.getPageCardsByQuery(Object.assign(params, { page }))
          )
        );

        const allPagesCards$ = merge(firstPageCards$, remainingPagesCards$);

        return iif(() => pageCount > 1, allPagesCards$, firstPageCards$);
      })
    );
  }

  /**
   * Retrieves a card page from the Scryfall API for the given parameters
   *
   * See {@link https://scryfall.com/docs/api/cards/search}
   *
   * @returns {Observable<Card>}
   */
  getPageByQuery(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<ScryfallList<Card>> {
    return this.httpClient
      .get<ScryfallList<ScryfallCardInterface>>(
        `${this.getEndpoint()}/search`,
        { params }
      )
      .pipe(
        map((response) =>
          Object.assign(response, {
            data: response.data.map(
              (card) =>
                new Card(
                  setFromMappings<ScryfallCardInterface, CardInterface>(
                    card,
                    CardInterfaceMappings
                  )
                )
            ),
          })
        )
      );
  }

  /**
   * Retrieves an Observable emitting all the cards of a given page, parsed into {@link Card}, from the Scryfall API for the given parameters
   *
   * See {@link https://scryfall.com/docs/api/cards/search}
   *
   * @returns {Observable<Card>}
   */
  getPageCardsByQuery(
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        }
  ): Observable<Card> {
    return this.httpClient
      .get<ScryfallList<ScryfallCardInterface>>(
        `${this.getEndpoint()}/search`,
        { params }
      )
      .pipe(
        mergeMap((response) =>
          response.data.map(
            (card) =>
              new Card(
                setFromMappings<ScryfallCardInterface, CardInterface>(
                  card,
                  CardInterfaceMappings
                )
              )
          )
        )
      );
  }

  /**
   * Retrieves Scryfall API endpoint for the cards resource
   *
   * @returns {string}
   */
  private getEndpoint(): string {
    return `${this.scryfallApiService.getEndpoint()}/cards`;
  }
}
