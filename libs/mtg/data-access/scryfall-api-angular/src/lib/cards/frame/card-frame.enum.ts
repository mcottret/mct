export enum CardFrame {
  Year1993 = '1993',
  Year1997 = '1997',
  Year2003 = '2003',
  Year2015 = '2015',
  Future = 'future',
}
