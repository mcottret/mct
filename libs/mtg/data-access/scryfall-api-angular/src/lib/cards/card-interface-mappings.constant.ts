import { $enum } from 'ts-enum-util';

import {
  CardType,
  CardSupertype,
  CardLayout,
  CardRarity,
  CardImageFormat,
  CardInterface,
  Mana,
  Card,
  SetCode,
} from '@mct/mtg/util/ts-models';
import { Mappings, setFromMappings } from '@mct/shared/util/object';

import { CardFace } from './card-face.interface';
import { CardLayout as ScryfallCardLayout } from './card-layout.enum';
import { CardRarity as ScryfallCardRarity } from './card-rarity.enum';
import { CardInterface as ScryfallCardInterface } from './card.interface';
import { CardImageFormat as ScryfallCardImageFormat } from './card-image-format.enum';

export const CardInterfaceMappings: Mappings<
  ScryfallCardInterface | CardFace,
  CardInterface
> = {
  oracle_text: 'oracleText',
  hand_modifier: 'handModifier',
  life_modifier: 'lifeModifier',
  color_identity: 'colorIdentity',

  layout: (card: ScryfallCardInterface) =>
    $enum(CardLayout).getValueOrDefault(
      $enum(ScryfallCardLayout).getKeyOrDefault(card.layout),
      card.layout
    ),

  rarity: (card: ScryfallCardInterface) =>
    $enum(CardRarity).getValueOrDefault(
      $enum(ScryfallCardRarity).getKeyOrDefault(card.rarity),
      card.rarity
    ),

  set: {
    key: 'setCode',
    value: (card: ScryfallCardInterface) =>
      card.set === SetCode.StrixhavenMysticalArchive
        ? SetCode.Strixhaven
        : card.set,
  },

  mana_cost: {
    key: 'manaCost',
    value: (card) => new Mana(card.mana_cost),
  },

  card_faces: {
    key: 'faces',
    value: (card: ScryfallCardInterface) =>
      card.card_faces.map(
        (cardFace) =>
          new Card(
            setFromMappings<CardFace, CardInterface>(
              cardFace,
              CardInterfaceMappings
            )
          )
      ),
  },

  released_at: {
    key: 'releaseDate',
    value: (card: ScryfallCardInterface) =>
      new Date(`${card.released_at}T00:00:00-08:00`),
    default: new Date(0),
  },

  image_uris: {
    key: 'imagesUris',
    value: (card) => {
      const mappedImagesUris = {};

      for (const format in card.image_uris) {
        if (card.image_uris.hasOwnProperty(format)) {
          const mappedFormat = $enum(CardImageFormat).getValueOrDefault(
            $enum(ScryfallCardImageFormat).getKeyOrDefault(format),
            format
          );

          mappedImagesUris[mappedFormat] = new URL(card.image_uris[format]);
        }
      }

      return mappedImagesUris;
    },
  },

  type_line: [
    'typeLine',
    {
      key: 'types',
      value: (card) =>
        card.type_line
          .split('—')[0]
          .split(' ')
          .filter((typeOrSupertype) =>
            $enum(CardType).isValue(typeOrSupertype)
          ),
    },
    {
      key: 'subtypes',
      value: (card) => {
        const split = card.type_line.split('—');

        return split[1] ? split[1].split(' ') : [];
      },
    },
    {
      key: 'supertypes',
      value: (card) =>
        card.type_line
          .split('—')[0]
          .split(' ')
          .filter((typeOrSupertype) =>
            $enum(CardSupertype).isValue(typeOrSupertype)
          ),
    },
  ],
};
