import { RelatedCardComponent } from './related-card-component.enum';

export interface RelatedCard {
  id: string; // UUID
  uri: string; // URI
  name: string;
  type_line: string;
  object: 'related_card';

  component: RelatedCardComponent;
}
