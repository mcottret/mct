export enum RelatedCardComponent {
  Token = 'token',
  MeldPart = 'meld_part',
  MeldResult = 'meld_result',
  ComboPiece = 'combo_piece',
}
