export enum CardLegality {
  Legal = 'legal',
  Banned = 'banned',
  NotLegal = 'not_legal',
  Restricted = 'restricted',
}
