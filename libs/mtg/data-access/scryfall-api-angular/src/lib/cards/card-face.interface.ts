import { ColorSymbol } from '@mct/mtg/util/ts-models';

import { CardImageFormat } from './card-image-format.enum';

export interface CardFace {
  name: string;
  object: 'card_face';
  mana_cost: string;
  type_line: string;

  power?: string;
  artist?: string;
  colors?: ColorSymbol[]; // https://scryfall.com/docs/api/colors
  loyalty?: string;
  watermark?: string;
  toughness?: string;
  oracle_text?: string;
  flavor_text?: string;
  printed_name?: string;
  printed_text?: string;
  color_indicator?: ColorSymbol[]; // https://scryfall.com/docs/api/colors
  illustration_id?: string; // UUID
  printed_type_line?: string;

  image_uris?: {
    [format in CardImageFormat]: string; // URI
  };
}
