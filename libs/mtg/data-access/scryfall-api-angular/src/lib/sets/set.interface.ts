import { SetType } from './set-type.enum';

export interface SetInterface {
  id: string; // UUID
  uri: string; // URI
  name: string;
  code: string;
  digital: boolean;
  foil_only: boolean;
  card_count: number;
  search_uri: string; // URI
  icon_svg_uri: string; // URI
  scryfall_uri: string; // URI

  set_type: SetType;

  block?: string;
  mtgo_code?: string;
  block_code?: string;
  released_at?: string; // YYYY-MM-DD - GMT-8 Pacific time
  tcgplayer_id?: number;
  parent_set_code?: string;
}
