import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SetsHttpService } from './sets-http.service';

describe('SetsHttpService', () => {
  let service: SetsHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(SetsHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
