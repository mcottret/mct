import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { setFromMappings } from '@mct/shared/util/object';
import { Set, SetInterface } from '@mct/mtg/util/ts-models';

import { ScryfallList } from '../scryfall-list.interface';
import { ScryfallApiService } from '../scryfall-api.service';
import { SetInterfaceMappings } from './set-interface-mappings.constant';
import { SetInterface as ScryfallSetInterface } from './set.interface';

@Injectable({
  providedIn: 'root',
})
export class SetsHttpService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly scryfallApiService: ScryfallApiService
  ) {}

  /**
   * Retrieves an Observable emitting all the sets from the Scryfall API, parsed into Set
   *
   * @returns {Observable<Set>}
   */
  getAll(): Observable<Set> {
    return this.httpClient
      .get<ScryfallList<ScryfallSetInterface>>(this.getEndpoint())
      .pipe(
        mergeMap((response) =>
          response.data.map(
            (set) =>
              new Set(
                setFromMappings<ScryfallSetInterface, SetInterface>(
                  set,
                  SetInterfaceMappings
                )
              )
          )
        )
      );
  }

  /**
   * Retrieves Scryfall API endpoint for the sets resource
   *
   * @returns {string}
   */
  private getEndpoint(): string {
    return `${this.scryfallApiService.getEndpoint()}/sets`;
  }
}
