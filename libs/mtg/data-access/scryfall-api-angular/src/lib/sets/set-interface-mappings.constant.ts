import { $enum } from 'ts-enum-util';

import { Mappings } from '@mct/shared/util/object';
import { SetCode, SetInterface, SetType } from '@mct/mtg/util/ts-models';

import { SetType as ScryfallSetType } from './set-type.enum';
import { SetInterface as ScryfallSetInterface } from './set.interface';

export const SetInterfaceMappings: Mappings<
  ScryfallSetInterface,
  SetInterface
> = {
  card_count: 'cardCount',
  block_code: 'blockCode',

  search_uri: [
    {
      key: 'searchUri',
      value: (set) => new URL(set.search_uri),
    },
    {
      key: 'draftBoosterSearchUri',
      value: (set) => {
        const uri = new URL(set.search_uri);
        const searchParams = uri.searchParams;

        /**
         * Scryfall sets' default search URI may include duplicates, cards not found in draft boosters, or non-English prints
         *
         * See:
         *  - {@link https://scryfall.com/docs/syntax}
         *  - {@link https://scryfall.com/docs/api/cards/search}
         */
        searchParams.delete('unique');
        searchParams.set('q', `${searchParams.get('q')} is:booster`);

        /**
         * Cards from the {@link https://scryfall.com/sets/sta Strixhaven Mystical Archive} set can appear in {@link https://scryfall.com/sets/stx Strixhaven}
         *  draft boosters, but Scryfall doesn't include them by default as it considers the two sets to be distinct.
         *
         * See {@link https://magic.wizards.com/en/articles/archive/feature/collecting-strixhaven-school-mages-2021-03-25}
         **/
        if (set.code === SetCode.Strixhaven) {
          searchParams.set(
            'q',
            `((${searchParams.get('q')}) or e:${
              SetCode.StrixhavenMysticalArchive
            })`
          );
        }

        searchParams.set('q', `(${searchParams.get('q')} lang:en)`);

        return uri;
      },
    },
  ],

  released_at: {
    key: 'releaseDate',
    value: (set) => new Date(`${set.released_at}T00:00:00-08:00`),
    default: new Date(0),
  },

  set_type: {
    key: 'type',
    value: (set) =>
      $enum(SetType).getValueOrDefault(
        $enum(ScryfallSetType).getKeyOrDefault(set.set_type),
        set.set_type
      ),
  },
};
