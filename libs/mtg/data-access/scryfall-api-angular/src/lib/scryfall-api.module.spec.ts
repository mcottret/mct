import { TestBed, waitForAsync } from '@angular/core/testing';

import { ScryfallApiModule } from './scryfall-api.module';

describe('ScryfallApiModule', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [ScryfallApiModule],
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(ScryfallApiModule).toBeDefined();
  });
});
