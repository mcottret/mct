export const ScryfallApi = {
  baseUrl: 'https://api.scryfall.com',
  maximumCardsPageSize: 175,
};
