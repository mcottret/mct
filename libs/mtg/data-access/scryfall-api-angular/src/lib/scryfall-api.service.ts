import { Injectable } from '@angular/core';

import { ScryfallApi } from './scryfall-api.constant';

@Injectable({
  providedIn: 'root',
})
export class ScryfallApiService {
  constructor() {}

  /**
   * Retrieves the base Scryfall API endpoint
   *
   * @returns {string}
   */
  getEndpoint(): string {
    return `${ScryfallApi.baseUrl}`;
  }
}
