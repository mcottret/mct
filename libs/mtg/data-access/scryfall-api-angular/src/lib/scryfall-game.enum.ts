export enum ScryfallGame {
  Mtgo = 'mtgo',
  Paper = 'paper',
  Arena = 'arena',
}
