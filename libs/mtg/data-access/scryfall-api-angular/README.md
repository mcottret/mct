# [Scryfall API](https://scryfall.com/docs/api) [Angular](https://angular.io/)

[Scryfall API](https://scryfall.com/docs/api) [Angular](https://angular.io/) is an [Angular](https://angular.io/) wrapper for the [Scryfall API](https://scryfall.com/docs/api).

## Dependencies

_Modules:_

- [`HttpClientModule`](https://angular.io/guide/http)

## Usage

To use [Scryfall API](https://scryfall.com/docs/api) [Angular](https://angular.io/), import the `ScryfallApiModule` from `@mct/mtg/data-access/scryfall-api-angular` in your requiring module as in the example below:

```typescript
import { HttpClientModule } from '@angular/common/http';

import { ScryfallApiModule } from '@mct/mtg/data-access/scryfall-api-angular';

@NgModule({
  imports: [HttpClientModule, ScryfallApiModule],
})
export class AppModule {}
```

You may also need to import `HttpClientModule` from your application module if it does not already.
