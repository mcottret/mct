import { Mapping } from './mapping.type';

export interface Mappings<T, U> {
  [key: string]: Mapping<T> | Mapping<T>[];
}
