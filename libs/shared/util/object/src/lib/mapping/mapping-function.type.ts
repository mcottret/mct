/**
 * A data mapping function, returning a dynamic new value for an object's key
 *
 * See:
 *  - {@link Mapping}
 *  - {@link setFromMappings}
 *  - {@link https://en.wikipedia.org/wiki/Data_mapping}
 */
export type MappingFunction<T> = (object: T) => any;
