import { MappingFunction } from './mapping-function.type';

/**
 * A data mapping object, defining a new value (or setting an optional default one for the given key if it does not exist
 *  in the source object) & an optional new key for the key / value pair of an object
 */
export interface MappingObject<T> {
  key?: string;
  value: MappingFunction<T>;
  default?: any;
}
