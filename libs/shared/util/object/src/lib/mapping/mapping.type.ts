import { MappingObject } from './mapping-object.interface';
import { MappingFunction } from './mapping-function.type';

/**
 * A data mapping value defining a transformation for an object's key, possible values & transformations rules are as
 *  follows:
 *
 *  - a {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String string}, being a
 *    new key for the original value in the source object (/!\ this may erase another existing key /!\)
 *  - a {@link MappingObject}, defining a new value (or setting an optional default one for the given key if it does not
 *    exist in the source object) & an optional new key for the key / value pair in the source object (/!\ this may erase
 *    another existing key /!\)
 *  - a {@link MappingFunction}, dynamically defining a new value for the original key in the source object
 *
 * See:
 *  - {@link setFromMappings}
 *  - {@link https://en.wikipedia.org/wiki/Data_mapping}
 */
export type Mapping<T> = string | MappingObject<T> | MappingFunction<T>;
