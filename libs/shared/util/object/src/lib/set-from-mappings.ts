import { Mappings } from '@mct/shared/util/object';

import { Mapping } from './mapping/mapping.type';

/**
 * Sets the given object properties according to given {@link Mappings} & returns the modified object
 *
 * The {@link Mappings} indicate each key that needs to be modified in the source object, for each matching key, the
 *  associated {@link Mapping} (or {@link Mapping[]}) value(s) indicate the transformation(s) to apply to the source
 *  object for that key, see {@link Mapping} for possible transformations & rules
 *
 * T: the type of the source object, it constraints the type of the {@link Mappings} parameter
 * U: the new type of the returned modified object
 *
 * See {@link https://en.wikipedia.org/wiki/Data_mapping}
 *
 * @param object
 * @param mappings
 */
export function setFromMappings<T, U>(object: T, mappings: Mappings<T, U>): U {
  for (const key in mappings) {
    if (mappings.hasOwnProperty(key)) {
      let keyMappings = mappings[key];

      if (!Array.isArray(keyMappings)) {
        keyMappings = [keyMappings];
      }

      let shouldKeepKey = false;

      keyMappings.forEach((mapping) => {
        object = setFromMapping<T>(object, key, mapping);

        if (!shouldKeepKey) {
          shouldKeepKey =
            mapping === key ||
            typeof mapping === 'function' ||
            (typeof mapping === 'object' && mapping.key === key);
        }
      });

      // Deletes the original key from the source object if it's no longer needed
      if (!shouldKeepKey) {
        delete object[key];
      }
    }
  }

  return object as T & U;
}

/**
 * Applies the given {@link Mapping} transformation to the given object for the given key, see {@link Mapping} for
 *  possible transformations & rules
 *
 * T: the type of the source object, it constraints the type of the {@link Mapping} object
 *
 * See {@link https://en.wikipedia.org/wiki/Data_mapping}
 *
 * @param object
 * @param key
 * @param mapping
 */
function setFromMapping<T>(object: T, key: string, mapping: Mapping<T>): T {
  if (typeof mapping === 'object') {
    const mappedKey = mapping.key ?? key;

    if (object.hasOwnProperty(key)) {
      object[mappedKey] = mapping.value(object);
    } else if (mapping.default) {
      object[mappedKey] = mapping.default;
    }
  } else if (object.hasOwnProperty(key)) {
    if (typeof mapping === 'function') {
      object[key] = mapping(object);
    } else {
      object[mapping] = object[key];
    }
  }

  return object;
}
