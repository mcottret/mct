/**
 * Returns the result of the subtraction of B from A (A - B), meaning returning A from which all the values present in B have been
 *  removed. Since this operates on {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array arrays}
 *  & not {@link https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Set Sets}, A & B may contain
 *  duplicated values, so the subtraction counts every occurrence of same values in both arrays as a distinct value,
 *  unlike most array difference algorithms
 *
 * @param A
 * @param B
 */
export function subtract(A: string[], B: string[]): string[] {
  return A.filter(
    function (value) {
      return !this.get(value) || !this.set(value, this.get(value) - 1);
    },
    B.reduce(
      (accumulator, currentValue) =>
        accumulator.set(currentValue, (accumulator.get(currentValue) ?? 0) + 1),
      new Map()
    )
  );
}
