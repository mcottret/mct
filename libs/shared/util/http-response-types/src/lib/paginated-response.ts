export interface PaginatedResponse<T> {
  count: number;
  pageSize: number;
  totalCount: number;

  data?: T[];
}
