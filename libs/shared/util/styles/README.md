# Shared Styles

Shared Styles is an utility library exposing generic mixins, variables, components, layouts & themes stylesheets.

## Usage

To use Shared Styles, add its include path to your application `angular.json`'s section `stylePreprocessorOptions` `includePaths` array like in the example below:

```json
{
  "projects": {
    "my-application": {
      "architect": {
        "build": {
          "options": {
            "stylePreprocessorOptions":{
              "includePaths": [
                "libs/shared/util/styles/src/lib"
              ]
            },
            "extractCss": true,
            ...
          },
          ...
        },
        ...
      },
      ...
    }
  },
  ...
}
```

You may also need to set the `extractCss` option to `true` if it is not already.

Then, import its `shared-main.scss` file from your application's `styles.scss` file like in the example below:

```scss
@use "shared-main.scss";
// ...
```
