import { Component } from '@angular/core';

@Component({
  selector: 'mct-mtg-playing-around-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
