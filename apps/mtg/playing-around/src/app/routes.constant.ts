import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@mct/mtg/feature/playing-around').then(
        (m) => m.PlayingAroundModule
      ),
  },
  { path: '**', pathMatch: 'full', redirectTo: '' },
];
