# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/mcottret/mct/compare/v1.1.0...v1.2.0) (2021-08-12)


### Features

* **mtg/playing-around:** add query parameters matching the selected colors symbols & CMC ([9c5fa80](https://gitlab.com/mcottret/mct/commit/9c5fa80611dc20a369586e371b4e2ff41936d32e))
* **mtg/playing-around:** implement selection of multiple sets ([ba8b92a](https://gitlab.com/mcottret/mct/commit/ba8b92a347aeb7f50941fcdf4569022b20272862))

## [1.1.0](https://gitlab.com/mcottret/mct/compare/v1.0.0...v1.1.0) (2021-04-29)


### Features

* **mtg/playing-around:** add a `setCode` query parameter matching the selected set ([262cfb8](https://gitlab.com/mcottret/mct/commit/262cfb837beb94898fbb8681ca9fe00019e05f94))
* **mtg/playing-around:** handle foretell costs when calculating cards playability ([d98aa77](https://gitlab.com/mcottret/mct/commit/d98aa778425fc0b484298327bea9fd0ae0fc6d3e))
* **mtg/playing-around:** handle Mystical Archive cards appearing in Strixhaven draft boosters ([d342bf0](https://gitlab.com/mcottret/mct/commit/d342bf0b2ff7100d3e680889f330b58dd2740b15))
* **mtg/playing-around:** rotate split cards 90° when zoomed ([beff238](https://gitlab.com/mcottret/mct/commit/beff238673ef7b26eee608a06295692dfb672a87))

## 1.0.0 (2020-11-22)


### Features

* **mtg:** create the api-angular lib & implement the `GET /sets` route ([2658781](https://gitlab.com/mcottret/mct/commit/2658781e81f9a1c46d809c69680a5b4d635d81d9))
* **mtg:** create the mana-selector UI library ([0136ef3](https://gitlab.com/mcottret/mct/commit/0136ef337ea312e59ac36bf12574c2f4f8caf144))
* **mtg:** create the playing-around application shell ([b9b13c4](https://gitlab.com/mcottret/mct/commit/b9b13c4bdfc736ad99360e93a73d47c254c57910))
* **mtg:** create the playing-around feature library ([efa306a](https://gitlab.com/mcottret/mct/commit/efa306a8153b1e4e55d402c50ebc6de8c6d43f81))
* **mtg/api-angular:** implement the `GET /cards` route ([5fd9fba](https://gitlab.com/mcottret/mct/commit/5fd9fba7f8cd14487414b5a52c2ae4c5b4060297))
* **mtg/playing-around:** finalize the first version ([1121acb](https://gitlab.com/mcottret/mct/commit/1121acbaae7b8e8077c5613c998f1cbffede59f1))
* **mtg/playing-around:** implement a draft sets selector ([78ee73c](https://gitlab.com/mcottret/mct/commit/78ee73c1d70ea48b2bb846997961c7148e4f6da5))
* **mtg/playing-around:** implement filtering of cards by playability & cycling playability ([a6ed424](https://gitlab.com/mcottret/mct/commit/a6ed424b59361cd09fc3dffa67c0456c6a6902c3))
* **mtg/playing-around:** implement first filtering of cards by instant type ([f2ceb06](https://gitlab.com/mcottret/mct/commit/f2ceb060d97b27d3b82877a26876476a749144a0))
* **mtg/playing-around:** improve filtering of cards ([6a18ab1](https://gitlab.com/mcottret/mct/commit/6a18ab1120095b04260ece3972405c7e105c8181))
* **mtg/playing-around:** setup the PWA ([af7091e](https://gitlab.com/mcottret/mct/commit/af7091ec5c7cc673e57ca789fb567eaaa9402ee5))


### Bug Fixes

* **mtg/playing-around:** fix card retrieval errors ([4a62b0b](https://gitlab.com/mcottret/mct/commit/4a62b0b4c7ee62597a05caf1b1de4214d6e25ef1))
