# mct

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

Projects monorepo built with [Nx](https://nx.dev), a set of extensible dev tools for monorepos (check the [Nx documentation](https://nx.dev) to learn more).

## Architecture

Following an architecture inspired by the [Entreprise Angular Monorepo Patterns Book](https://connect.nrwl.io/app/books/enterprise-angular-monorepo-patterns), this project is composed of the following groups, each containing its related applications & libraries:

- `mtg`: applications & libraries related to ["Magic: The Gathering"](https://magic.wizards.com/en)

  _Applications:_

  - [`playing-around`](./apps/mtg/playing-around/README.md): the ["MTG: Playing Around"](https://mtg.gamepedia.com/List_of_Magic_slang#P) [Angular](https://angular.io/) [progressive web application](https://en.wikipedia.org/wiki/Progressive_web_application)

  _Libraries:_

  - _Feature_

    - [`playing-around`](./libs/mtg/feature/playing-around/README.md): the ["MTG: Playing Around"](https://mtg.gamepedia.com/List_of_Magic_slang#P) [Angular](https://angular.io/) feature library

  - _Data Access_

    - [`api-angular`](./libs/mtg/data-access/api-angular/README.md): the [MTG API](https://magicthegathering.io/) [Angular](https://angular.io/) wrapper
    - [`scryfall-api-angular`](./libs/mtg/data-access/scryfall-api-angular/README.md): the [Scryfall API](https://scryfall.com/docs/api) [Angular](https://angular.io/) wrapper

  - _UI_

    - [`set`](./libs/mtg/ui/set/README.md): set of UI components related to [MTG sets](https://mtg.gamepedia.com/Set)
    - [`mana`](./libs/mtg/ui/mana/README.md): set of UI components related to [MTG mana](https://mtg.gamepedia.com/Mana)
    - [`card`](./libs/mtg/ui/card/README.md): set of UI components related to [MTG cards](https://mtg.gamepedia.com/Card)

  - _Util_

    - [`assets`](./libs/mtg/util/assets/README.md): set of MTG assets files & utilities
    - [`styles`](./libs/mtg/util/styles/README.md): MTG-related mixins, variables, components, layouts & themes stylesheets
    - [`ts-models`](./libs/mtg/util/ts-models/README.md): set of [Typescript](https://www.typescriptlang.org/) classes to map MTG entities (cards, sets ...)

- `shared`: shared libraries

  _Libraries:_

  - Util

    - [`map`](./libs/shared/util/map/README.md): generic utilities for [JS Maps](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
    - [`array`](./libs/shared/util/array/README.md): generic utilities for [JS Arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
    - [`object`](./libs/shared/util/object/README.md): generic utilities for [JS Objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)
    - [`http-response-types`](./libs/shared/util/http-response-types/README.md): types for generic HTTP response shapes (eg: paginated, rate limited ...)
    - [`styles`](./libs/shared/util/styles/README.md): generic mixins, variables, components, layouts & themes stylesheets
